exit

set ORACLE_SID=ELNT11
set NLS_LANG=AMERICAN_AMERICA.AL32UTF8


echo "starting full database from to import ..." >>  imp_ELNT11.log
date /t >> imp_ELNT11.log
time /t >> imp_ELNT11.log


rem sqlplus system/Testsolvias2012

rem imp userid=system/Testsolvias2012 parfile=imp_csccartridge.par commit=y ignore=y statistics=none file=exp_elnp_full_2012-08-17.dmp log=imp_elnt11_full_csccartridge_2012-08-23.log

rem Dauer : 2h
rem imp userid=system/Testsolvias2012 parfile=imp_cs_notebook9.par indexes=n commit=y ignore=y statistics=none file=exp_elnp_full_2012-08-17.dmp log=imp_elnt11_full_csnotebook9_2012-08-24.log


rem imp userid=system/Testsolvias2012 parfile=imp_rest.par indexes=n commit=y ignore=y statistics=none file=exp_elnp_full_2012-08-17.dmp log=imp_elnt11_fromto_2012-08-24.log


echo "full database from to import finished." >> imp_ELNT11.log
date /t >> imp_ELNT11.log
time /t >> imp_ELNT11.log


pause