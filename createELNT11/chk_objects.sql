-- spool chk_objects_index_so-s2030-elnp.log
spool chk_objects_index_so-s3130-elnt11.log

col host_name format a20
col username  format a20
col tablespace_name format a25
col temporary_tablespace format a25
col object_name format A40
col object_type format a20
col owner format A25
col stmt format a100
col sequence_owner format a30
col sequence_name  format a30
col table_owner    format a20
col table_name     format a30
col synonym_name   format a25


set pages 0
set lines 120


select instance_name,host_name,version from v$instance;

prompt list tablespaces ...
select tablespace_name from dba_tablespaces order by tablespace_name;

prompt list temp files ...
select tablespace_name from dba_temp_files order by tablespace_name asc;

prompt list schemas ...
select username,default_tablespace, temporary_tablespace from dba_users order by username;


prompt list schemas having segments ...
select distinct owner from dba_segments order by owner;


prompt list invalid objects ...
select owner,object_type,object_name
from dba_objects where status != 'VALID'
order by owner,object_type,object_name;


prompt list sequences ...
select sequence_owner,sequence_name from dba_sequences order by 1,2;

prompt list public synonyms ...
select owner,synonym_name,table_owner,table_name from dba_synonyms where owner='PUBLIC' 
and table_owner not in ('SYS','SYSTEM','MDSYS','ORDSYS','SYSMAN','XDB','WMSYS','FLOWS_FILES','EXFSYS','ORDDATA','CTXSYS','OLAPSYS','APPQOSSYS')
and table_owner not like 'APEX%'
order by 1,2,3,4;


prompt cnt objects not sys, system ...
select owner,object_type,count(1) anz
from dba_objects where owner not in ('SYS','SYSTEM','OUTLN','WMSYS','XDB','CTXSYS'
                                                                                   ,'DBSNMP','DIP','TSMSYS','MDSYS','ORDSYS','ORDPLUGINS','MGMT_VIEW','EXFSYS','DMSYS','ANONYMOUS','SI_INFORMTN_SCHEMA'
																				   ,'SYSMAN')
group by owner,object_type
order by owner,object_type;

prompt list objects not sys, system, without indices ...
select owner,object_type,object_name 
from dba_objects ob
where
object_type not in ('INDEX') 
and owner not in ('SYS','SYSTEM','OUTLN','WMSYS','XDB','CTXSYS'
                                                                                   ,'DBSNMP','DIP','TSMSYS','MDSYS','ORDSYS','ORDPLUGINS','MGMT_VIEW','EXFSYS','DMSYS','ANONYMOUS','SI_INFORMTN_SCHEMA'
																				   ,'SYSMAN')
order by owner asc,object_type asc,object_name asc;


prompt list objects not sys, system ...
select owner,object_type,object_name 
from dba_objects ob
where owner not in ('SYS','SYSTEM','OUTLN','WMSYS','XDB','CTXSYS'
                                                                                   ,'DBSNMP','DIP','TSMSYS','MDSYS','ORDSYS','ORDPLUGINS','MGMT_VIEW','EXFSYS','DMSYS','ANONYMOUS','SI_INFORMTN_SCHEMA'
																				   ,'SYSMAN')
order by owner asc,object_type asc,object_name asc;


prompt cnt objects sys, system ...
select owner,object_type,count(1) anz
from dba_objects where owner in ('SYS','SYSTEM','OUTLN','WMSYS','XDB','CTXSYS'
                                                                                   ,'DBSNMP','DIP','TSMSYS','MDSYS','ORDSYS','ORDPLUGINS','MGMT_VIEW','EXFSYS','DMSYS','ANONYMOUS','SI_INFORMTN_SCHEMA'
																				   ,'SYSMAN')
group by owner,object_type
order by owner,object_type;

prompt list objects sys, system ...
select owner,object_type,object_name 
from dba_objects where owner in ('SYS','SYSTEM','OUTLN','WMSYS','XDB','CTXSYS'
                                                                                   ,'DBSNMP','DIP','TSMSYS','MDSYS','ORDSYS','ORDPLUGINS','MGMT_VIEW','EXFSYS','DMSYS','ANONYMOUS','SI_INFORMTN_SCHEMA'
																				   ,'SYSMAN')
order by owner,object_type;



spool off;

