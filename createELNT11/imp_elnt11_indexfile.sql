

set ORACLE_SID=ELNT11
set NLS_LANG=AMERICAN_AMERICA.AL32UTF8


echo "starting full indexfile creation ..." >>  imp_ELNT11_indexfile.log
date /t >> imp_ELNT11_indexfile.log
time /t >> imp_ELNT11_indexfile.log


rem sqlplus system/Testsolvias2012

imp userid=system/Testsolvias2012 rows=n full=y indexes=n constraints=n statistics=none indexfile=elnt_indexfile.sql file=exp_elnp_full_2012-08-17.dmp log=imp_elnt11_indexfile_2012-08-20.log


echo "full database indexfile finished." >> imp_ELNT11_indexfile.log
date /t >> imp_ELNT11_indexfile.log
time /t >> imp_ELNT11_indexfile.log


pause