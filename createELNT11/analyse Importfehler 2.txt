SQL> select owner,object_type,object_name
  2  from dba_objects where status != 'VALID'
  3  order by owner,object_type,object_name;
CS_NOTEBOOK9                   VIEW                ENV_TABLE_CELLS
PUBLIC                         SYNONYM             CHEM_REG_PRIVILEGES
PUBLIC                         SYNONYM             PEOPLE_PROJECT

CS_NOTEBOOK9                   22050.1875
CS_NOTEBOOK9_AUDIT                16703.5
CS_LTA                          5598.5625
SYS                              677.3125
CSCARTRIDGE                      225.8125
REGDB                            144.6875
SYSMAN                               78.5
XDB                               48.0625
SYSTEM                            22.0625
MDSYS                              21.875
CS_SECURITY                        14.875
CTXSYS                             9.5625
WMSYS                               6.875
EXFSYS                              3.625
CSCUSER                            1.8125
DBSNMP                             1.5625
ORDSYS                                 .5
OUTLN                                  .5
DMSYS                                 .25
TSMSYS                                .25

set lines 120
set pages 0
col object_name format a30
col stmt format a100

select owner,object_type,object_name
from dba_objects where status != 'VALID'
order by owner,object_type,object_name;





set lines 120
col object_name format a30
select 'alter view '|| owner ||'.'||object_name||' compile;' stmt
from dba_objects where status != 'VALID'
and object_type='VIEW'
order by owner,object_type,object_name;

alter view CS_NOTEBOOK9.ENV_COLLECTIONS compile;
alter view CS_NOTEBOOK9.ENV_COLLECTION_USES compile;
alter view CS_NOTEBOOK9.ENV_REFERENCES compile;
alter view CS_NOTEBOOK9.ENV_TRANSITION_TYPE_USES compile;
alter view CS_NOTEBOOK9.ENV_USER_ACCESS_2 compile;


select owner,sum(bytes)/1024/1024 mb
from dba_segments
group by owner
order by 2 desc;

CS_NOTEBOOK9                   18596.3125
SYS                                 917.5
XDB                              157.6875
APEX_030200                        85.375
MDSYS                             73.9375
SYSMAN                              45.75
SYSTEM                            29.6875
ORDDATA                           13.4375
OLAPSYS                             5.125
CTXSYS                              3.875
EXFSYS                              3.625
WMSYS                                 3.5
OUTLN                               .5625
CSCARTRIDGE                         .5625
ORDSYS                              .4375
SCOTT                               .3125
DBSNMP                              .1875

-- invalid objects after imp_rest.par
CS_NOTEBOOK9              SYNONYM              ENA_SEARCHES
CS_NOTEBOOK9              SYNONYM              ENA_SEARCH_LOCATIONS
CS_NOTEBOOK9              SYNONYM              ENA_SECTIONS
CS_NOTEBOOK9              SYNONYM              ENA_SECTION_LISTENERS
CS_NOTEBOOK9              SYNONYM              ENA_SECTION_SETS
CS_NOTEBOOK9              SYNONYM              ENA_SECTION_TYPES
CS_NOTEBOOK9              SYNONYM              ENA_SECTION_TYPE_SETS
CS_NOTEBOOK9              SYNONYM              ENA_SPECTRA
CS_NOTEBOOK9              SYNONYM              ENA_STATES
CS_NOTEBOOK9              SYNONYM              ENA_STATE_QUERIES
CS_NOTEBOOK9              SYNONYM              ENA_STYLED_TEXTS
CS_NOTEBOOK9              SYNONYM              ENA_STYLED_TEXT_FIELDS
CS_NOTEBOOK9              SYNONYM              ENA_SUBSECTION_SECTION_SETS
CS_NOTEBOOK9              SYNONYM              ENA_SUBSECTION_SECTION_TYPES
CS_NOTEBOOK9              SYNONYM              ENA_TABLES
CS_NOTEBOOK9              SYNONYM              ENA_TABLE_CELLS
CS_NOTEBOOK9              SYNONYM              ENA_TABLE_PROPERTIES
CS_NOTEBOOK9              SYNONYM              ENA_TABLE_ROWS
CS_NOTEBOOK9              SYNONYM              ENA_TRANSITION_LISTENERS
CS_NOTEBOOK9              SYNONYM              ENA_TRANSITION_TYPES
CS_NOTEBOOK9              SYNONYM              ENA_TRANSITION_TYPE_USES
CS_NOTEBOOK9              SYNONYM              ENA_URLDISPLAYS
CS_NOTEBOOK9              SYNONYM              ENCCA_LIBRARIES
CS_NOTEBOOK9              SYNONYM              ENCCA_REACTANTS
CS_NOTEBOOK9              SYNONYM              ENCCA_REACTIONS

cd C:\oracle\product\11.2.0\dbhome_1\RDBMS\ADMIN
sqlplus / nolog
connect / as sysdba
@utlrp

PL/SQL procedure successfully completed.


TIMESTAMP
----------------------------------------------------------------------------

COMP_TIMESTAMP UTLRP_END  2012-08-24 12:46:39

DOC> The following query reports the number of objects that have compiled
DOC> with errors (objects that compile with errors have status set to 3 in
DOC> obj$). If the number is higher than expected, please examine the error
DOC> messages reported with each object (using SHOW ERRORS) to see if they
DOC> point to system misconfiguration or resource constraints that must be
DOC> fixed before attempting to recompile these objects.
DOC>#

OBJECTS WITH ERRORS
-------------------
                  0

DOC> The following query reports the number of errors caught during
DOC> recompilation. If this number is non-zero, please query the error
DOC> messages in the table UTL_RECOMP_ERRORS to see if any of these errors
DOC> are due to misconfiguration or resource constraints that must be
DOC> fixed before objects can compile successfully.
DOC>#

ERRORS DURING RECOMPILATION
---------------------------
                          0


Function created.


PL/SQL procedure successfully completed.


Function dropped.


PL/SQL procedure successfully completed.


PL/SQL procedure successfully completed.

SQL> set lines 120
SQL> set pages 0
SQL> col object_name format a30
SQL> col stmt format a100
SQL>
SQL> select owner,object_type,object_name
  2  from dba_objects where status != 'VALID'
  3  order by owner,object_type,object_name;

no rows selected

SQL>

