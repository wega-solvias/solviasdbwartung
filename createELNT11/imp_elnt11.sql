

set ORACLE_SID=ELNT11
set NLS_LANG=AMERICAN_AMERICA.AL32UTF8


echo "starting full dataase import ..." >>  imp_ELNT11.log
date /t >> imp_ELNT11.log
time /t >> imp_ELNT11.log


rem sqlplus system/TestSolvias2012

imp userid=system/TestSolvias2012 parfile=imp.par commit=y ignore=y file=exp_elnp_full_2012-08-17.dmp log=imp_elnt11_full_2012-08-20.log


echo "full database import finished." >> imp_ELNT11.log
date /t >> imp_ELNT11.log
time /t >> imp_ELNT11.log


pause