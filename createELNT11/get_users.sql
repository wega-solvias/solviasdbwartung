set serveroutput on size 1000000
SET LONG 2000000
declare
 
 cursor cur_user is select username, created from dba_users where username not in ('SYS','SYSTEM','OUTLN','WMSYS','XDB','CTXSYS'
                                                                                   ,'DBSNMP','DIP','TSMSYS','MDSYS','ORDSYS','ORDPLUGINS','MGMT_VIEW','EXFSYS','DMSYS','ANONYMOUS','SI_INFORMTN_SCHEMA'
																				   ,'SYSMAN')						
 order by created desc;
 
 cursor cur_profiles is select distinct profile from dba_profiles;
 
 cursor cur_roles is select role from dba_roles where role  in (
 'BROWSER'
,'CSS_USER'
,'CHEMICAL_ADMINISTRATOR'
,'CSS_ADMIN'
,'CSS_USER'
,'SUBMITTER'
,'SUPERVISING_CHEMICAL_ADMIN'
,'PERFUME_CHEMIST'
,'SUPERVISING_SCIENTIST'
,'CS_NOTEBOOK9_USER'
,'CS_LTA_USER_ROLE'
,'TECHNICIAN'
);
 
 mychar varchar2(1);
 p      clob;
 l_user varchar2(555);

 
 procedure trimanoutputclob(i_clob in clob) is
   l_temp varchar2(5000) := null;
 begin
   l_temp := i_clob;

   
   l_temp := ltrim(rtrim(l_temp));
   dbms_output.put_line(l_temp);
   
   --for i in 1 .. 100 loop
   --  mychar := substr(i_clob,i,1);
   --  dbms_output.put_line(i || ':'|| mychar ||':'|| ascii(mychar) );
   --  null;
   --end loop;
   
 end;
 
 procedure get_userinfo(i_user in varchar2,i_created in date) is
   c clob;
 begin
   dbms_output.put_line('-- #########################################');
   dbms_output.put_line('-- '||i_user||' created : '|| to_char(i_created,'dd.mm.yyyy'));
   dbms_output.put_line('--');
 
 
  c := DBMS_METADATA.GET_DDL ('USER',i_user);  
  trimanoutputclob(c);

  begin
    c := DBMS_METADATA.GET_GRANTED_DDL ('SYSTEM_GRANT' ,i_user);
    dbms_output.put_line(c);  
  exception
    when others then
      dbms_output.put_line('-- '||i_user||' : role object not found');
  end;


  begin
    c := DBMS_METADATA.GET_GRANTED_DDL ('ROLE_GRANT' ,i_user);
    dbms_output.put_line(c);  
  exception
    when others then
      dbms_output.put_line('-- '||i_user||' : role object not found');
  end;
  
 end;
 
begin
  for i in cur_user loop
    l_user := l_user || i.username || ',';
  end loop;
  l_user := substr(l_user,1,length(l_user) -1 );
  dbms_output.put_line('FROMUSER='||l_user);
  dbms_output.put_line('TOUSER  ='  ||l_user);
  
 for i in cur_profiles loop
   dbms_output.put_line('-- #########################################');
   dbms_output.put_line('-- '||i.profile);
   dbms_output.put_line('--');
   if i.profile != 'DEFAULT' then
     p := DBMS_METADATA.GET_DDL ('PROFILE',i.profile);  
     dbms_output.put_line(p);
   end if;
 end loop;
 
 for i in cur_roles loop
   dbms_output.put_line('-- #########################################');
   dbms_output.put_line('-- '||i.role);
   dbms_output.put_line('--');
   p := DBMS_METADATA.GET_DDL ('ROLE',i.role);  
   dbms_output.put_line(p);
   begin
     p := DBMS_METADATA.GET_GRANTED_DDL ('ROLE_GRANT' ,i.role);
     dbms_output.put_line(p); 
   exception
     when others then
       dbms_output.put_line('-- no objects granted to role');
   end;
   
 end loop;
 
 
 
  
  for i in cur_user loop
    get_userinfo(i.username,i.created);
  end loop;
  
  
end;
/