-- #########################################                                    
-- CSUSERPROFILE                                                                
--                                                                              
CREATE PROFILE "CSUSERPROFILE" LIMIT 
COMPOSITE_LIMIT DEFAULT 
SESSIONS_PER_USER DEFAULT 
CPU_PER_SESSION DEFAULT 
CPU_PER_CALL DEFAULT 
LOGICAL_READS_PER_SESSION DEFAULT 
LOGICAL_READS_PER_CALL DEFAULT 
IDLE_TIME DEFAULT 
CONNECT_TIME DEFAULT 
PRIVATE_SGA DEFAULT 
FAILED_LOGIN_ATTEMPTS UNLIMITED 
PASSWORD_LIFE_TIME UNLIMITED 
PASSWORD_REUSE_TIME UNLIMITED 
PASSWORD_REUSE_MAX UNLIMITED 
PASSWORD_VERIFY_FUNCTION NULL 
PASSWORD_LOCK_TIME 900/86400 
PASSWORD_GRACE_TIME UNLIMITED; 


alter user ctxsys account unlock;
                                                       
-- #########################################                                    
-- CSS_ADMIN                                                                    
--                                                                              
CREATE ROLE "CSS_ADMIN";
GRANT "CONNECT" TO "CSS_ADMIN";

-- #########################################                                    
-- CSS_USER                                                                     
--                                                                              
CREATE ROLE "CSS_USER";
GRANT "CONNECT" TO "CSS_USER";

-- #########################################                                    
-- BROWSER                                                                      
--                                                                              
CREATE ROLE "BROWSER";
GRANT "CONNECT" TO "BROWSER";
GRANT "CSS_USER" TO "BROWSER";
-- #########################################                                    
-- CHEMICAL_ADMINISTRATOR                                                       
--                                                                              
CREATE ROLE "CHEMICAL_ADMINISTRATOR";
GRANT "CONNECT" TO "CHEMICAL_ADMINISTRATOR";
GRANT "CSS_USER" TO        "CHEMICAL_ADMINISTRATOR";

-- #########################################                                    
-- SUBMITTER                                                                    
--                                                                              
CREATE ROLE "SUBMITTER";
GRANT "CONNECT" TO "SUBMITTER";
GRANT "CSS_USER" TO "SUBMITTER";
        
-- #########################################                                    
-- SUPERVISING_CHEMICAL_ADMIN                                                   
--                                                                              
CREATE ROLE "SUPERVISING_CHEMICAL_ADMIN";
GRANT "CONNECT" TO "SUPERVISING_CHEMICAL_ADMIN";
GRANT "CSS_ADMIN" TO   "SUPERVISING_CHEMICAL_ADMIN";


-- #########################################                                    
-- CS_NOTEBOOK9_USER                                                            
--                                                                              

CREATE ROLE "CS_NOTEBOOK9_USER";
GRANT "CONNECT" TO "CS_NOTEBOOK9_USER";
                                     
-- #########################################                                    
-- CS_LTA_USER_ROLE                                                             
--                                                                              

CREATE ROLE "CS_LTA_USER_ROLE";

-- #########################################                                    
-- SUPERVISING_SCIENTIST                                                        
--                                                                              
CREATE ROLE "SUPERVISING_SCIENTIST";
GRANT "CONNECT" TO "SUPERVISING_SCIENTIST";
GRANT "CSS_USER" TO         "SUPERVISING_SCIENTIST";
                               
-- #########################################                                    
-- PERFUME_CHEMIST                                                              
--                                                                              
CREATE ROLE "PERFUME_CHEMIST";
GRANT "CONNECT" TO "PERFUME_CHEMIST";
GRANT "CSS_USER" TO               "PERFUME_CHEMIST";


-- #########################################                                    
-- TECHNICIAN                                                                   
--                                                                              
CREATE ROLE "TECHNICIAN";
GRANT "CSS_USER" TO "TECHNICIAN";






-- #########################################                                    
-- FUCHSBE1 created : 03.01.2012                                                
--                                                                              

CREATE USER "FUCHSBE1" IDENTIFIED BY VALUES '9BA5D243DE749AF9'
DEFAULT
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- FUCHSBE1 : role object not found                                             

GRANT "CONNECT" TO "FUCHSBE1";
GRANT "BROWSER" TO "FUCHSBE1";
           
-- #########################################                                    
-- LAUEKL1 created : 03.01.2012                                                 
--                                                                              

CREATE USER "LAUEKL1" IDENTIFIED BY VALUES '9D48446B38196364'
DEFAULT 
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- LAUEKL1 : role object not found                                              

GRANT "CONNECT" TO "LAUEKL1";
GRANT "BROWSER" TO "LAUEKL1";
             
-- #########################################                                    
-- FLEMIMA1 created : 17.02.2010                                                
--                                                                              

CREATE USER "FLEMIMA1" IDENTIFIED BY VALUES '29780D65D0E78548'
DEFAULT
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- FLEMIMA1 : role object not found                                             

GRANT "CONNECT" TO "FLEMIMA1";
GRANT "BROWSER" TO "FLEMIMA1";
           
-- #########################################                                    
-- BERNAGI4 created : 24.04.2009                                                
--                                                                              

CREATE USER "BERNAGI4" IDENTIFIED BY VALUES '75AEEFDD4A7920E1'
DEFAULT
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- BERNAGI4 : role object not found                                             

GRANT "CONNECT" TO "BERNAGI4";
GRANT "BROWSER" TO "BERNAGI4";
           
-- #########################################                                    
-- HAHNDI1 created : 07.04.2009                                                 
--                                                                              

CREATE USER "HAHNDI1" IDENTIFIED BY VALUES '059BF13AADC62C44'
DEFAULT 
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- HAHNDI1 : role object not found                                              

GRANT "CONNECT" TO "HAHNDI1";
GRANT "BROWSER" TO "HAHNDI1";
             
-- #########################################                                    
-- WORTHJE1 created : 16.02.2009                                                
--                                                                              

CREATE USER "WORTHJE1" IDENTIFIED BY VALUES '716B461E39265CFE'
DEFAULT
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- WORTHJE1 : role object not found                                             

GRANT "CONNECT" TO "WORTHJE1";
GRANT "CSS_USER" TO "WORTHJE1";
GRANT "CHEMICAL_ADMINISTRATOR" TO "WORTHJE1";
GRANT "SUPERVISING_CHEMICAL_ADMIN"  TO "WORTHJE1";
                                                                  
-- #########################################                                    
-- BORNAN9 created : 10.02.2009                                                 
--                                                                              

CREATE USER "BORNAN9" IDENTIFIED BY VALUES '443D917EA8BCAD0F'
DEFAULT 
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- BORNAN9 : role object not found                                              

GRANT "CONNECT" TO "BORNAN9";
GRANT "BROWSER" TO "BORNAN9";
             
-- #########################################                                    
-- DEPONRE1 created : 09.02.2009                                                
--                                                                              

CREATE USER "DEPONRE1" IDENTIFIED BY VALUES '03FF4BF1DF8E29E6'
DEFAULT
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- DEPONRE1 : role object not found                                             

GRANT "CONNECT" TO "DEPONRE1";
GRANT "BROWSER" TO "DEPONRE1";
           
-- #########################################                                    
-- LOTTEWI2 created : 05.02.2009                                                
--                                                                              

CREATE USER "LOTTEWI2" IDENTIFIED BY VALUES '1D94E4B147100336'
DEFAULT
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- LOTTEWI2 : role object not found                                             

GRANT "CONNECT" TO "LOTTEWI2";
GRANT "BROWSER" TO "LOTTEWI2";
           
-- #########################################                                    
-- PUPOWDO1 created : 11.11.2008                                                
--                                                                              

CREATE USER "PUPOWDO1" IDENTIFIED BY VALUES 'F3D72E17661E5A44'
DEFAULT
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- PUPOWDO1 : role object not found                                             

GRANT "CONNECT" TO "PUPOWDO1";
GRANT "BROWSER" TO "PUPOWDO1";
           
-- #########################################                                    
-- HECKMTH1 created : 21.10.2008                                                
--                                                                              

CREATE USER "HECKMTH1" IDENTIFIED BY VALUES '61BF9A604CFDC0BA'
DEFAULT
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- HECKMTH1 : role object not found                                             

GRANT "CONNECT" TO "HECKMTH1";
GRANT "BROWSER" TO "HECKMTH1";
           
-- #########################################                                    
-- LANDEHE2 created : 16.10.2008                                                
--                                                                              

CREATE USER "LANDEHE2" IDENTIFIED BY VALUES '4BFEFFC40FD77E14'
DEFAULT
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- LANDEHE2 : role object not found                                             

GRANT "CONNECT" TO "LANDEHE2";
GRANT "BROWSER" TO "LANDEHE2";
           
-- #########################################                                    
-- PUGINBE2 created : 16.10.2008                                                
--                                                                              

CREATE USER "PUGINBE2" IDENTIFIED BY VALUES 'A7CCE6D66F499B3E'
DEFAULT
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- PUGINBE2 : role object not found                                             

GRANT "CONNECT" TO "PUGINBE2";
GRANT "BROWSER" TO "PUGINBE2";
           
-- #########################################                                    
-- SPINDFE2 created : 16.10.2008                                                
--                                                                              

CREATE USER "SPINDFE2" IDENTIFIED BY VALUES '88B817F0D2BEACB3'
DEFAULT
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- SPINDFE2 : role object not found                                             

GRANT "CONNECT" TO "SPINDFE2";
GRANT "CSS_USER" TO "SPINDFE2";
GRANT "CSS_ADMIN" TO "SPINDFE2";
GRANT "BROWSER" TO "SPINDFE2";
GRANT         "CHEMICAL_ADMINISTRATOR" TO "SPINDFE2";
GRANT "SUPERVISING_CHEMICAL_ADMIN"  TO "SPINDFE2";
                                                                  
-- #########################################                                    
-- BAPPEER1 created : 16.10.2008                                                
--                                                                              

CREATE USER "BAPPEER1" IDENTIFIED BY VALUES '0F281C021836F762'
DEFAULT
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- BAPPEER1 : role object not found                                             

GRANT "CONNECT" TO "BAPPEER1";
GRANT "BROWSER" TO "BAPPEER1";
           
-- #########################################                                    
-- STEBLTH1 created : 16.10.2008                                                
--                                                                              

CREATE USER "STEBLTH1" IDENTIFIED BY VALUES '318D231E765619FB'
DEFAULT
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- STEBLTH1 : role object not found                                             

GRANT "CONNECT" TO "STEBLTH1";
GRANT "BROWSER" TO "STEBLTH1";
           
-- #########################################                                    
-- SCHMIWO4 created : 29.09.2008                                                
--                                                                              

CREATE USER "SCHMIWO4" IDENTIFIED BY VALUES 'A69668EA706FA97A'
DEFAULT
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- SCHMIWO4 : role object not found                                             

GRANT "CONNECT" TO "SCHMIWO4";
GRANT "BROWSER" TO "SCHMIWO4";
           
-- #########################################                                    
-- FURERPA1 created : 29.09.2008                                                
--                                                                              

CREATE USER "FURERPA1" IDENTIFIED BY VALUES 'AE8E56C65AEA58EC'
DEFAULT
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- FURERPA1 : role object not found                                             

GRANT "CONNECT" TO "FURERPA1";
GRANT "CSS_USER" TO "FURERPA1";
GRANT "CHEMICAL_ADMINISTRATOR" TO "FURERPA1";
GRANT "SUPERVISING_CHEMICAL_ADMIN"  TO "FURERPA1";
                                                                  
-- #########################################                                    
-- LAUKA1 created : 07.07.2008                                                  
--                                                                              

CREATE USER "LAUKA1" IDENTIFIED BY VALUES 'E063AC1F7371C326'
DEFAULT  
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- LAUKA1 : role object not found                                               

GRANT "CONNECT" TO "LAUKA1";
GRANT "BROWSER" TO "LAUKA1";
               
-- #########################################                                    
-- EMMENRE1 created : 01.07.2008                                                
--                                                                              

CREATE USER "EMMENRE1" IDENTIFIED BY VALUES '29BAC8E627380ED2'
DEFAULT
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- EMMENRE1 : role object not found                                             

GRANT "CONNECT" TO "EMMENRE1";
GRANT "BROWSER" TO "EMMENRE1";
           
-- #########################################                                    
-- MUELLMA1 created : 16.06.2008                                                
--                                                                              

CREATE USER "MUELLMA1" IDENTIFIED BY VALUES '1A942A45F05DF794'
DEFAULT
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- MUELLMA1 : role object not found                                             

GRANT "CONNECT" TO "MUELLMA1";
GRANT "BROWSER" TO "MUELLMA1";
           
-- #########################################                                    
-- SPIELDI1 created : 03.06.2008                                                
--                                                                              

CREATE USER "SPIELDI1" IDENTIFIED BY VALUES '4BC4B6CF177DCC04'
DEFAULT
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- SPIELDI1 : role object not found                                             

GRANT "CONNECT" TO "SPIELDI1";
GRANT "BROWSER" TO "SPIELDI1";
           
-- #########################################                                    
-- NETTEUL1 created : 30.05.2008                                                
--                                                                              

CREATE USER "NETTEUL1" IDENTIFIED BY VALUES '61B68772D9A4F92B'
DEFAULT
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- NETTEUL1 : role object not found                                             

GRANT "CONNECT" TO "NETTEUL1";
GRANT "BROWSER" TO "NETTEUL1";
           
-- #########################################                                    
-- SCHNEHE1 created : 20.05.2008                                                
--                                                                              

CREATE USER "SCHNEHE1" IDENTIFIED BY VALUES '466B68BB33C30604'
DEFAULT
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- SCHNEHE1 : role object not found                                             

GRANT "CONNECT" TO "SCHNEHE1";
GRANT "BROWSER" TO "SCHNEHE1";
           
-- #########################################                                    
-- WICKERE1 created : 23.04.2008                                                
--                                                                              

CREATE USER "WICKERE1" IDENTIFIED BY VALUES '14D3DEA1880914F7'
DEFAULT
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- WICKERE1 : role object not found                                             

GRANT "CONNECT" TO "WICKERE1";
GRANT "BROWSER" TO "WICKERE1";
           
-- #########################################                                    
-- RIECKKA1 created : 23.04.2008                                                
--                                                                              

CREATE USER "RIECKKA1" IDENTIFIED BY VALUES '6847F77591CE262F'
DEFAULT
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- RIECKKA1 : role object not found                                             

GRANT "CONNECT" TO "RIECKKA1";
GRANT "BROWSER" TO "RIECKKA1";
           
-- #########################################                                    
-- MOESSCH1 created : 15.04.2008                                                
--                                                                              

CREATE USER "MOESSCH1" IDENTIFIED BY VALUES 'C8F77D56E441900F'
DEFAULT
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- MOESSCH1 : role object not found                                             

GRANT "CONNECT" TO "MOESSCH1";
GRANT "BROWSER" TO "MOESSCH1";
           
-- #########################################                                    
-- BOUDIAN1 created : 14.04.2008                                                
--                                                                              

CREATE USER "BOUDIAN1" IDENTIFIED BY VALUES '362CB9648341215F'
DEFAULT
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- BOUDIAN1 : role object not found                                             

GRANT "CONNECT" TO "BOUDIAN1";
GRANT "CSS_USER" TO "BOUDIAN1";
GRANT "CSS_ADMIN" TO "BOUDIAN1";
GRANT "CHEMICAL_ADMINISTRATOR" TO "BOUDIAN1";
GRANT "SUPERVISING_CHEMICAL_ADMIN" TO "BOUDIAN1";
                               
-- #########################################                                    
-- T2_85 created : 14.04.2008                                                   
--                                                                              

CREATE USER "T2_85" IDENTIFIED BY VALUES '9E1E52EBB1D416E5'
DEFAULT   
TABLESPACE "T_REGDB_TABL"
TEMPORARY TABLESPACE "TEMP";
                    
-- T2_85 : role object not found                                                
GRANT "SUBMITTER" TO "T2_85";
                                               
-- #########################################                                    
-- T6_84 created : 14.04.2008                                                   
--                                                                              

CREATE USER "T6_84" IDENTIFIED BY VALUES '5BA5FC71E1ABFDB7'
DEFAULT   
TABLESPACE "T_REGDB_TABL"
TEMPORARY TABLESPACE "TEMP";
                    
-- T6_84 : role object not found                                                

GRANT "PERFUME_CHEMIST" TO "T6_84";
                                         
-- #########################################                                    
-- T3_84 created : 14.04.2008                                                   
--                                                                              

CREATE USER "T3_84" IDENTIFIED BY VALUES 'F1FB8D3128DF4CCF'
DEFAULT   
TABLESPACE "T_REGDB_TABL"
TEMPORARY TABLESPACE "TEMP";
                    
-- T3_84 : role object not found                                                

GRANT "SUPERVISING_SCIENTIST" TO "T3_84";
                                   
-- #########################################                                    
-- T3_85 created : 14.04.2008                                                   
--                                                                              

CREATE USER "T3_85" IDENTIFIED BY VALUES '0094061CD76442C6'
DEFAULT   
TABLESPACE "T_REGDB_TABL"
TEMPORARY TABLESPACE "TEMP";
                    
-- T3_85 : role object not found                                                

GRANT "SUPERVISING_SCIENTIST" TO "T3_85";
                                   
-- #########################################                                    
-- T5_85 created : 14.04.2008                                                   
--                                                                              

CREATE USER "T5_85" IDENTIFIED BY VALUES '8A753EF91C18852B'
DEFAULT   
TABLESPACE "T_REGDB_TABL"
TEMPORARY TABLESPACE "TEMP";
                    
-- T5_85 : role object not found                                                

GRANT "SUPERVISING_CHEMICAL_ADMIN" TO "T5_85";
                              
-- #########################################                                    
-- T4_85 created : 14.04.2008                                                   
--                                                                              

CREATE USER "T4_85" IDENTIFIED BY VALUES 'F5FDDD7D647977A7'
DEFAULT   
TABLESPACE "T_REGDB_TABL"
TEMPORARY TABLESPACE "TEMP";
                    
-- T4_85 : role object not found                                                

GRANT "CHEMICAL_ADMINISTRATOR" TO "T4_85";
                                  
-- #########################################                                    
-- T5_84 created : 14.04.2008                                                   
--                                                                              

CREATE USER "T5_84" IDENTIFIED BY VALUES '3AEC72E22EAC8AE5'
      DEFAULT   
TABLESPACE "T_REGDB_TABL"
      TEMPORARY TABLESPACE "TEMP";
                    
-- T5_84 : role object not found                                                

GRANT "SUPERVISING_CHEMICAL_ADMIN" TO "T5_84";
                              
-- #########################################                                    
-- T2_84 created : 14.04.2008                                                   
--                                                                              

CREATE USER "T2_84" IDENTIFIED BY VALUES '6149DCC3937AFFD2'
DEFAULT   
TABLESPACE "T_REGDB_TABL"
TEMPORARY TABLESPACE "TEMP";
                    
-- T2_84 : role object not found                                                

GRANT "SUBMITTER" TO "T2_84";
                                               
-- #########################################                                    
-- T4_84 created : 14.04.2008                                                   
--                                                                              

CREATE USER "T4_84" IDENTIFIED BY VALUES 'CE8B4ED3E3E27794'
DEFAULT   
TABLESPACE "T_REGDB_TABL"
TEMPORARY TABLESPACE "TEMP";
                    
-- T4_84 : role object not found                                                

GRANT "CHEMICAL_ADMINISTRATOR" TO "T4_84";
                                  
-- #########################################                                    
-- T6_85 created : 14.04.2008                                                   
--                                                                              

CREATE USER "T6_85" IDENTIFIED BY VALUES '6B6BBC85FA8EC421'
DEFAULT   
TABLESPACE "T_REGDB_TABL"
TEMPORARY TABLESPACE "TEMP";
                    
-- T6_85 : role object not found                                                

   GRANT "PERFUME_CHEMIST" TO "T6_85";
                                         
-- #########################################                                    
-- T1_85 created : 14.04.2008                                                   
--                                                                              

CREATE USER "T1_85" IDENTIFIED BY VALUES '24E8B29CEB5F585A'
DEFAULT   
TABLESPACE "T_REGDB_TABL"
TEMPORARY TABLESPACE "TEMP";
                    
-- T1_85 : role object not found                                                

GRANT "BROWSER" TO "T1_85";
                                                 
-- #########################################                                    
-- T1_84 created : 14.04.2008                                                   
--                                                                              

CREATE USER "T1_84" IDENTIFIED BY VALUES 'F8203A2597FB95F3'
DEFAULT   
TABLESPACE "T_REGDB_TABL"
TEMPORARY TABLESPACE "TEMP";
                    
-- T1_84 : role object not found                                                

GRANT "BROWSER" TO "T1_84";
                                                 
-- #########################################                                    
-- REGDB created : 14.04.2008                                                   
--                                                                              

CREATE USER "REGDB" IDENTIFIED BY VALUES '636FB156037EB478'
DEFAULT   
TABLESPACE "T_REGDB_TABL"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP";
      

GRANT CREATE ANY MATERIALIZED VIEW TO "REGDB";
GRANT CREATE ANY VIEW TO   "REGDB";
 
GRANT CREATE ANY INDEX TO "REGDB";
GRANT SELECT ANY TABLE TO     "REGDB";
 
GRANT UNLIMITED TABLESPACE TO "REGDB";

GRANT "CONNECT" TO "REGDB";
GRANT "RESOURCE" TO "REGDB";
                
-- #########################################                                    
-- LOTZMA1 created : 09.04.2008                                                 
--                                                                              

CREATE USER "LOTZMA1" IDENTIFIED BY VALUES '1935AC866B5669FB'
DEFAULT 
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP";
PROFILE "CSUSERPROFILE";
                                                        
-- LOTZMA1 : role object not found                                              

GRANT "CONNECT" TO "LOTZMA1";
GRANT "CSS_USER" TO "LOTZMA1";
GRANT  "CHEMICAL_ADMINISTRATOR" TO "LOTZMA1";
GRANT "SUPERVISING_CHEMICAL_ADMIN" TO "LOTZMA1";
                                                                      
-- #########################################                                    
-- FLUBADI1 created : 14.03.2008                                                
--                                                                              

CREATE USER "FLUBADI1" IDENTIFIED BY VALUES 'FBA03006233B8E42'
DEFAULT
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP"
PROFILE "CSUSERPROFILE";
                                                        
-- FLUBADI1 : role object not found                                             

GRANT "CONNECT" TO "FLUBADI1";
GRANT "CSS_USER" TO "FLUBADI1";
GRANT "CSS_ADMIN" TO "FLUBADI1";
GRANT "CHEMICAL_ADMINISTRATOR" TO "FLUBADI1";
GRANT "SUPERVISING_CHEMICAL_ADMIN" TO "FLUBADI1";
                               
-- #########################################                                    
-- TEST_USER2 created : 08.02.2008                                              
--                                                                              

CREATE USER "TEST_USER2" IDENTIFIED BY VALUES '323EC35CA1677057'
DEFAULT TABLESPACE "USERS"
TEMPORARY TABLESPACE "TEMP";
                   
-- TEST_USER2 : role object not found                                           

GRANT "CS_NOTEBOOK9_USER" TO "TEST_USER2";
                                  
-- #########################################                                    
-- TEST_USER created : 07.02.2008                                               
--                                                                              

CREATE USER "TEST_USER" IDENTIFIED BY VALUES 'C0A0F776EBBBB7FB'
DEFAULT TABLESPACE "USERS"
TEMPORARY TABLESPACE "TEMP";
                   
-- TEST_USER : role object not found                                            

GRANT "CS_NOTEBOOK9_USER" TO "TEST_USER";
                                   
-- #########################################                                    
-- CS_LTA created : 07.02.2008                                                  
--                                                                              

CREATE USER "CS_LTA" IDENTIFIED BY VALUES '7AC1E905AA84221A'
DEFAULT  
TABLESPACE "T_CS_LTA_TBL"
TEMPORARY TABLESPACE "TEMP";
                    

GRANT QUERY REWRITE TO "CS_LTA";
GRANT UNLIMITED TABLESPACE TO "CS_LTA";
GRANT "RESOURCE" TO "CS_LTA";
GRANT "CTXAPP" TO "CS_LTA";
               
-- #########################################                                    
-- CS_LTA_USER created : 07.02.2008                                             
--                                                                              

CREATE USER "CS_LTA_USER" IDENTIFIED BY VALUES 'AF3078B87DDE28DD'
DEFAULT TABLESPACE "USERS"
TEMPORARY TABLESPACE "TEMP";
                   

GRANT CREATE DATABASE LINK TO "CS_LTA_USER";
GRANT "CONNECT" TO "CS_LTA_USER";
GRANT "CS_LTA_USER_ROLE" TO           "CS_LTA_USER";
                                                                  
-- #########################################                                    
-- CS_LTA_ELN_ACCESS created : 07.02.2008                                       
--                                                                              

CREATE USER "CS_LTA_ELN_ACCESS" IDENTIFIED BY VALUES 'C356CFF92B7AB94A'
DEFAULT TABLESPACE "T_CS_NOTEBOOK9_NB"
TEMPORARY TABLESPACE "T_EN_TEMP";
  
-- CS_LTA_ELN_ACCESS : role object not found                                    

GRANT "CONNECT" TO "CS_LTA_ELN_ACCESS";
                                     
-- #########################################                                    
-- CS_NOTEBOOK9_AUDIT created : 07.02.2008                                      
--                                                                              

CREATE USER "CS_NOTEBOOK9_AUDIT" IDENTIFIED BY VALUES '4D3016714237BB8A'
DEFAULT TABLESPACE "TA_CS_NOTEBOOK9_NB"
      TEMPORARY TABLESPACE "T_EN_TEMP";
 

GRANT CREATE ANY TRIGGER TO "CS_NOTEBOOK9_AUDIT";
 
GRANT CREATE ANY SYNONYM TO "CS_NOTEBOOK9_AUDIT";
 
GRANT CREATE ANY TABLE TO "CS_NOTEBOOK9_AUDIT";
GRANT UNLIMITED TABLESPACE TO "CS_NOTEBOOK9_AUDIT";
GRANT "CONNECT" TO "CS_NOTEBOOK9_AUDIT";
GRANT "RESOURCE" TO            "CS_NOTEBOOK9_AUDIT";
                                                           
-- #########################################                                    
-- CS_NOTEBOOK9 created : 07.02.2008                                            
--                                                                              

CREATE USER "CS_NOTEBOOK9" IDENTIFIED BY VALUES 'BFC098F17C460C5B'
DEFAULT TABLESPACE "T_CS_NOTEBOOK9_NB"
TEMPORARY TABLESPACE "T_EN_TEMP";
  

GRANT QUERY REWRITE TO "CS_NOTEBOOK9";
GRANT CREATE ROLE TO               "CS_NOTEBOOK9";
GRANT CREATE ANY SEQUENCE TO "CS_NOTEBOOK9";
GRANT CREATE ANY VIEW TO "CS_NOTEBOOK9";
 
GRANT CREATE ANY SYNONYM TO "CS_NOTEBOOK9";
GRANT ALTER USER TO "CS_NOTEBOOK9";
GRANT CREATE USER TO "CS_NOTEBOOK9";
GRANT UNLIMITED TABLESPACE TO "CS_NOTEBOOK9";
GRANT "CONNECT" TO "CS_NOTEBOOK9" WITH ADMIN OPTION;
GRANT "RESOURCE" TO "CS_NOTEBOOK9";
GRANT "CTXAPP" TO "CS_NOTEBOOK9";
GRANT "CS_NOTEBOOK9_USER" TO "CS_NOTEBOOK9" WITH ADMIN OPTION;

-- only for a limited time
grant dba to cs_notebook9;
grant execute on ctxsys.ctx_ddl to cs_notebook9;
                        
-- #########################################                                    
-- CSSADMIN created : 06.02.2008                                                
--                                                                              

CREATE USER "CSSADMIN" IDENTIFIED BY VALUES 'A592AF1B22387664'
DEFAULT
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP";
     
-- CSSADMIN : role object not found                                             

GRANT "CSS_USER" TO "CSSADMIN";
GRANT "CSS_ADMIN" TO "CSSADMIN";
GRANT "CHEMICAL_ADMINISTRATOR" TO "CSSADMIN";
GRANT "SUPERVISING_CHEMICAL_ADMIN" TO "CSSADMIN";
                                     
-- #########################################                                    
-- CSSUSER created : 06.02.2008                                                 
--                                                                              
CREATE USER "CSSUSER" IDENTIFIED BY VALUES '70E41ABD2C0CF0D4'
DEFAULT 
TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE "T_CS_SECURITY_TEMP";
     
-- CSSUSER : role object not found                                              
GRANT "CSS_USER" TO "CSSUSER";
                                              
-- #########################################                                    
-- CS_SECURITY created : 06.02.2008                                             
--                                                                              

CREATE USER "CS_SECURITY" IDENTIFIED BY VALUES '4BA2A917C16083E8'
DEFAULT TABLESPACE "T_CS_SECURITY"
TEMPORARY TABLESPACE                   "T_CS_SECURITY_TEMP";
                                                           

GRANT SELECT ANY DICTIONARY TO "CS_SECURITY";
GRANT GRANT ANY PRIVILEGE TO "CS_SECURITY";
GRANT ALTER ANY ROLE TO "CS_SECURITY";
GRANT GRANT ANY ROLE TO "CS_SECURITY";
GRANT DROP ANY ROLE TO "CS_SECURITY";
GRANT CREATE ROLE TO "CS_SECURITY";
GRANT SELECT ANY TABLE TO "CS_SECURITY";
GRANT DROP USER TO "CS_SECURITY";
GRANT ALTER USER TO "CS_SECURITY";
GRANT CREATE USER TO "CS_SECURITY";
GRANT UNLIMITED TABLESPACE TO "CS_SECURITY";
 GRANT "CONNECT" TO "CS_SECURITY" WITH ADMIN OPTION;
 GRANT "RESOURCE" TO "CS_SECURITY";
                                                                  
-- #########################################                                    
-- CSCUSER created : 06.02.2008                                                 
--                                                                              

CREATE USER "CSCUSER" IDENTIFIED BY VALUES 'E28E2A3A43EC3129'
DEFAULT 
TABLESPACE "USERS"
TEMPORARY TABLESPACE "TEMP";
                           

GRANT CREATE TYPE TO "CSCUSER";
GRANT CREATE PROCEDURE TO "CSCUSER";
GRANT CREATE DATABASE LINK TO "CSCUSER";
GRANT CREATE ANY INDEX TO "CSCUSER";
GRANT SELECT ANY TABLE TO "CSCUSER";
GRANT CREATE TABLE TO "CSCUSER";
 
   
GRANT CREATE SESSION TO "CSCUSER";

GRANT UNLIMITED TABLESPACE TO "CSCUSER";

-- only for a limited time ...
grant dba to cscuser;
grant ctxapp to cscuser;

-- only for a limited time
grant execute on ctxsys.ctx_ddl to cscuser;

                                              
-- CSCUSER : role object not found                                              
-- #########################################                                    
-- CSCARTRIDGE created : 06.02.2008                                             
--                                                                              

CREATE USER "CSCARTRIDGE" IDENTIFIED BY VALUES 'AAB3073EA8FC592A'
DEFAULT TABLESPACE "T_CSCARTRIDGE"
TEMPORARY TABLESPACE "TEMP";
           

GRANT CREATE INDEXTYPE TO "CSCARTRIDGE";
GRANT CREATE OPERATOR TO         "CSCARTRIDGE";
GRANT CREATE LIBRARY TO "CSCARTRIDGE";
GRANT CREATE TYPE TO "CSCARTRIDGE";
GRANT CREATE PROCEDURE TO "CSCARTRIDGE";
GRANT CREATE      SEQUENCE TO "CSCARTRIDGE";
GRANT SELECT ANY TABLE TO "CSCARTRIDGE";
GRANT CREATE TABLE TO "CSCARTRIDGE";
GRANT CREATE SESSION TO "CSCARTRIDGE";

GRANT UNLIMITED TABLESPACE TO "CSCARTRIDGE";

        
-- CSCARTRIDGE : role object not found                                          
