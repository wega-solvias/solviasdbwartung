

set ORACLE_SID=ELNT11
set NLS_LANG=AMERICAN_AMERICA.AL32UTF8


echo "starting full datbase structure import ..." >>  imp_ELNT11_structure.log
date /t >> imp_ELNT11_structure.log
time /t >> imp_ELNT11_structure.log


rem sqlplus system/TestSolvias2012

imp userid=system/TestSolvias2012 rows=n full=y commit=y ignore=y indexes=n constraints=n statistics=none file=exp_elnp_full_2012-08-17.dmp log=imp_elnt11_full_structure_2012-08-20.log


echo "full database structure import finished." >> imp_ELNT11_structure.log
date /t >> imp_ELNT11_structure.log
time /t >> imp_ELNT11_structure.log


pause