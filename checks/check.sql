

--  Database Information Gathering for Solvias Databases
--  wega Informatik AG
--  $Revision: 233 $
--  $Date: 2016-07-21 14:10:03 +0200 (Do., 21 Jul 2016) $ $Author: hg $
--  $HeadURL: http://warba/repos/wega/kleinkunden/solvias/checks/check.sql $


-- usage : sqlplus /nolog
--         connect / as sysdba
--         spool <serverName>_<SID>.log
--         @check.sql <SID>
--         spool off


set lines 140
set heading on


prompt

prompt ########################################################################

prompt ###  General Instance Information

prompt ########################################################################

prompt
prompt Database Uptime ...
prompt
SELECT to_char(startup_time,'dd.mm.yyyy HH24:MI:SS') "DB Startup Time"
FROM sys.v_$instance;


select name,created,log_mode,open_mode from v$database;

col host_name format a40
select instance_name,host_name from v$instance;

col global_name format a30
select * from global_name;

select * from v$version;

set lines 120
col product format a50
col version format a60


select rtrim(substr(replace(banner,'TNS for ',''),1,instr(replace(banner,'TNS for ',''),':')-1)) os
from   v$version
where  banner like 'TNS for %';

SELECT PRODUCT, VERSION FROM SYS.PRODUCT_COMPONENT_VERSION;

select dbms_utility.port_string from dual;

select sid from v$mystat where rownum = 1;


declare
    l_version        varchar2(255);
    l_compatibility varchar2(255);
begin
    dbms_utility.db_version( l_version, l_compatibility );
    dbms_output.put_line( 'Database Version : '||l_version );
    dbms_output.put_line( 'Compablity  : ' ||l_compatibility );
end;
/

prompt ########################################################################

prompt ###  Tracing Information

prompt ########################################################################

prompt hostname ...
select host_name from v$instance;


col name format a20
col value format a60

prompt listing current tracefile
select name,value from v$diag_info where name = 'Diag Trace';


col schemaname format a20
col tracefile  format a60

prompt Listing Tracefiles ...
select schemaname,s.sid,p.tracefile
from v$session s, v$process p
where s.paddr = p.addr
and schemaname not in ('SYS','SYSTEM')
order by schemaname;


prompt

prompt ########################################################################

prompt ###  Memory Sizing

prompt ########################################################################

prompt sga information ...

show sga

set pages 30
col component format a30
select component
      ,round(current_size /1024/1024,2) current_size_mb
      ,round(granule_size/1024/1024 )   granule_size_mb
from V$SGA_DYNAMIC_COMPONENTS 
order by component;

col version format a20
select MEMORY_SIZE,MEMORY_SIZE_FACTOR,ESTD_DB_TIME,ESTD_DB_TIME_FACTOR ,version
from v$memory_target_advice order by memory_size;


prompt
prompt v$process memory pga ...
prompt
select 
sum(pga_used_mem) /1024/1024 pga_used
,sum(pga_alloc_mem) /1024/1024 pga_alloc
,sum(pga_max_mem ) /1024/1024 pga_max
from v$process;



SELECT dbms_result_cache.status() FROM dual;



prompt
prompt Application Caching relevant Parameters
prompt

set lines 120

col name format a40
col value format a30
col ISSYS_MODIFIABLE format a15

select name
,value/1024/1024 MB
,ISSYS_MODIFIABLE from v$parameter where name in
('db_cache_size'
,'db_cache_size'
,'db_keep_cache_size'   
,'db_recycle_cache_size'
,'result_cache_max_size'
,'result_cache_max_result'
,'sga_max_size'
,'sga_target'
,'memory_max_size'
,'memory_target'
,'pga_aggregate_target'
);

prompt
prompt SGAINFO Statistics
prompt

select name
,bytes/1024/1024/1024 GB
,resizeable from v$sgainfo
order by 2 desc;


set lines 120
col component format a30
prompt
prompt v$sga_dynamic_components
prompt V$SGA_DYNAMIC_COMPONENTS displays information about the dynamic components in SGA. 
prompt This view summarizes information based on all completed SGA resize operations that occurred after startup.
prompt

select component, current_size/1024/1024 MBCurrent
, min_size/1024/1024 MBMinSize
, user_specified_size/1024/1024 MBUserSpecified
from v$sga_dynamic_components;

prompt
prompt V$SGA_DYNAMIC_FREE_MEMORY displays information about the amount of 
prompt SGA memory available for future dynamic SGA resize operations.
prompt
select current_size/1024/1024 MBCurrentSize from v$sga_dynamic_free_memory;


set lines 150
col parameter format a25
col component format a20
prompt
prompt V$MEMORY_TARGET_ADVICE displays tuning advice for the MEMORY_TARGET initialization parameter.
prompt
select * from V$MEMORY_TARGET_ADVICE;


prompt The following views provide information about dynamic resize operations:
prompt
prompt V$SGA_RESIZE_OPS displays information about the last 800 completed SGA resize operations. 
prompt This does not include any operations currently in progress.
prompt
SELECT start_time,
       end_time,
       component,
       oper_type,
       oper_mode,
       parameter,
       ROUND(initial_size/1024/1204) AS initial_size_mb,
       ROUND(target_size/1024/1204) AS target_size_mb,
       ROUND(final_size/1024/1204) AS final_size_mb,
       status
FROM   v$sga_resize_ops
ORDER BY start_time;

-- V$MEMORY_CURRENT_RESIZE_OPS displays information about memory resize operations (both automatic and manual) which are currently in progress.
-- V$MEMORY_RESIZE_OPS displays information about the last 800 completed memory resize operations (both automatic and manual). This does not include in-progress operations.

prompt
prompt V$MEMORY_DYNAMIC_COMPONENTS displays information about the current sizes of all dynamically 
prompt tuned memory components, including the total sizes of the SGA and instance PGA.
select * from V$MEMORY_DYNAMIC_COMPONENTS;




prompt ### Shared Pool executions
prompt Executions - Total number of times this object has been executed
prompt http://vsbabu.org/oracle/sect13.html

set lines 150
col owner format a30
col object format a50
col executions format 999G999G999
select  OWNER,
  NAME||' - '||TYPE object,
  EXECUTIONS
from  v$db_object_cache
where   EXECUTIONS > 100 
and   type in ('PACKAGE','PACKAGE BODY','FUNCTION','PROCEDURE')
and   owner != 'SYSMAN'
and   owner != 'SYS'
and   owner != 'DBSNMP'
order   by owner,EXECUTIONS desc;








prompt ########################################################################

prompt ---- Undo Information ------

prompt ########################################################################

col "UNDO RETENTION [Sec]" format A25

prompt Calculate UNDO_RETENTION for given UNDO Tablespace ...

SELECT d.undo_size/(1024*1024) "ACTUAL UNDO SIZE [MByte]",
       SUBSTR(e.value,1,25) "UNDO RETENTION [Sec]",
       ROUND((d.undo_size / (to_number(f.value) *
       g.undo_block_per_sec))) "OPTIMAL UNDO RETENTION [Sec]"
  FROM (
       SELECT SUM(a.bytes) undo_size
          FROM v$datafile a,
               v$tablespace b,
               dba_tablespaces c
         WHERE c.contents = 'UNDO'
           AND c.status = 'ONLINE'
           AND b.name = c.tablespace_name
           AND a.ts# = b.ts#
       ) d,
       v$parameter e,
       v$parameter f,
       (
       SELECT MAX(undoblks/((end_time-begin_time)*3600*24))
              undo_block_per_sec
         FROM v$undostat
       ) g
WHERE e.name = 'undo_retention'
  AND f.name = 'db_block_size'
/



prompt Calculate Needed Undo Size for given Database Activity

SELECT d.undo_size/(1024*1024) "ACTUAL UNDO SIZE [MByte]",
       SUBSTR(e.value,1,25) "UNDO RETENTION [Sec]",
       (TO_NUMBER(e.value) * TO_NUMBER(f.value) *
       g.undo_block_per_sec) / (1024*1024) 
      "NEEDED UNDO SIZE [MByte]"
  FROM (
       SELECT SUM(a.bytes) undo_size
         FROM v$datafile a,
              v$tablespace b,
              dba_tablespaces c
        WHERE c.contents = 'UNDO'
          AND c.status = 'ONLINE'
          AND b.name = c.tablespace_name
          AND a.ts# = b.ts#
       ) d,
      v$parameter e,
       v$parameter f,
       (
       SELECT MAX(undoblks/((end_time-begin_time)*3600*24))
         undo_block_per_sec
         FROM v$undostat
       ) g
 WHERE e.name = 'undo_retention'
  AND f.name = 'db_block_size'
/



prompt ########################################################################

prompt ---- Clip ------

prompt ########################################################################

set serveroutput on
declare
  l_hint varchar2(1000) := null;


  l_total_alloc number(10,2);
  l_total_free  number(10,2);
  l_total_used  number(10,2);
  l_total_percent number(10,2);




  function database_buffer_hitratio(o_hint varchar2) return number
  is
    l_phys_reads number;
    l_log1reads number;
    l_log2reads number;
    l_hit       number;
  begin
    select value into l_phys_reads
    from   v$sysstat
    where  name = 'physical reads';


    select value into l_log1reads
    from   v$sysstat
    where  name = 'db block gets';


    select value into l_log2reads
    from   v$sysstat
    where  name = 'consistent gets';

    l_hit := 1 - (l_phys_reads / (l_log1reads + l_log2reads));

    if l_hit < 0.95 then
      l_hint := '  -> not ok, should be between 0.95 and 1';  
    else
      l_hint := '  -> ok, range between 0.95 and 1';
    end if;

    return l_hit;
  end;

  function library_cache_hitratio(o_hint varchar2) return number
  is
    l_hit       number;
  begin

   select 1-(sum(reloads)/sum(pins)) into l_hit
   from   v$librarycache;

    if l_hit < 0.95 then
      l_hint := '  -> not ok, should be between 0.95 and 1';  
    else
      l_hint := '  -> ok, range between 0.95 and 1';
    end if;

    return l_hit;
  end;

  function datadictionary_cache_hitratio(o_hint varchar2) return number
  is
    l_hit       number;
  begin

    select 1-(sum(getmisses)/sum(gets)) into l_hit
    from   v$rowcache;

    if l_hit < 0.95 then
      l_hint := '  -> not ok, should be between 0.95 and 1';  
    else
      l_hint := '  -> ok, range between 0.95 and 1';
    end if;

    return l_hit;
  end;



begin
  dbms_output.put_line('-------------------------------------------------------------------------------');
  dbms_output.put_line('Database Buffer Cache Hit Ratio        : ' || round(database_buffer_hitratio(l_hint),6)|| l_hint) ;
  dbms_output.put_line('Library Cache Hit Ratio                : ' || round(library_cache_hitratio(l_hint),6)|| l_hint) ;
  dbms_output.put_line('Datadictionary Cache Hit Ratio         : ' || round(datadictionary_cache_hitratio(l_hint),6)|| l_hint) ;
  dbms_output.put_line('-------------------------------------------------------------------------------');

  select round(sum(bytes)/1024/1024/1024,2) into l_total_alloc  from dba_data_files;
  select round(sum(bytes)/1024/1024/1024,2) into l_total_used   from dba_segments;
  select round(sum(bytes)/1024/1024/1024,2) into l_total_free   from dba_free_space;
  l_total_percent := round(l_total_used / l_total_alloc,2)* 100;

  dbms_output.put_line('-------------------------------------------------------------------------------');
  dbms_output.put_line('Total space allocated        : ' ||  l_total_alloc ||' GB') ;
  dbms_output.put_line('Total space used             : ' ||  l_total_used ||' GB') ;
  dbms_output.put_line('Total space free (be careful): ' ||  l_total_free ||' GB') ;
  dbms_output.put_line('Total space percentage filled: ' ||  l_total_percent ||' %') ;
  dbms_output.put_line('-------------------------------------------------------------------------------');
  dbms_output.put_line('-------------------------------------------------------------------------------');

end;
/



set pages 50

col tablespace_name format a30
col mballoc format 999999.9
col mbfree  format 999999.9
col percentfree format 99.99
col Datenbank format A10

select
 '&1' DB
,to_char(sysdate,'dd.mm.yyyy') Datum 
,rpad(tablespace_name,30,' ') tablespace_name
,mba mballoc
,mbf mbfree
,percentfree 
from (
select x.*, round((mbf/mba) * 100,2) percentFree
from (select tablespace_name
,(select round(sum(bytes)/1024/1024,2) from dba_data_files f where f.TABLESPACE_NAME = tab.tablespace_name) MBA 
,(select round(sum(bytes)/1024/1024,2) from dba_free_space free where free.TABLESPACE_NAME = tab.tablespace_name) MBF
from dba_tablespaces tab ) x
) order by percentfree asc
/


select 
  tablespace_name
, round(bytes/1024/1024,0) MB
,maxbytes/1024/1024 maxMB
,user_bytes/1024/1024 userMB
,autoextensible 
from dba_data_files order by tablespace_name,file_id
/


prompt

prompt ########################################################################

prompt ###  END CLIP

prompt ########################################################################




prompt

prompt ########################################################################

prompt ###  RMAN

prompt ########################################################################


set lines 150

col operation format a50
col status format a21
col mb_processed format 99999


prompt rman backup history ... 
select operation,status,mbytes_processed,start_time,end_time from v$rman_status where trunc(start_time)>trunc(sysdate-15) order by start_time asc;

COL hrs    FORMAT 999.99
SELECT SESSION_KEY, INPUT_TYPE, STATUS,
       TO_CHAR(START_TIME,'mm/dd/yy hh24:mi') start_time,
       TO_CHAR(END_TIME,'mm/dd/yy hh24:mi')   end_time,
       ELAPSED_SECONDS/3600                   hrs
FROM V$RMAN_BACKUP_JOB_DETAILS
ORDER BY SESSION_KEY;


COL in_sec FORMAT a10
COL out_sec FORMAT a10
COL TIME_TAKEN_DISPLAY FORMAT a10
SELECT SESSION_KEY, 
       OPTIMIZED, 
       COMPRESSION_RATIO, 
       INPUT_BYTES_PER_SEC_DISPLAY in_sec,
       OUTPUT_BYTES_PER_SEC_DISPLAY out_sec, 
       TIME_TAKEN_DISPLAY
FROM   V$RMAN_BACKUP_JOB_DETAILS
ORDER BY SESSION_KEY;


prompt rman errors occured ...

select to_char(START_TIME,�DD MON YY HH24:Mi�) START_TIME ,STATUS,OPERATION
from v$rman_status where STATUS like �%ERROR%�
and start_time > sysdate - 60
order by 1 desc;

select * from v$database_block_corruption;








prompt

prompt ########################################################################

prompt ###  Tablespaces

prompt ########################################################################



col maxname format a62
select name maxName,round(bytes/1024/1024/1024,2) GB 
from (select name
                          ,bytes
                          ,max(bytes) over () maxbytes from v$datafile) 
where maxbytes=bytes;



col file_name format a62
col mb format 99999
select tablespace_name,file_name,bytes/1024/1024 mb ,autoextensible , maxbytes/1024/1024 maxGB
,status
from dba_data_files order by tablespace_name,file_name;

select file#,status from v$datafile where status not in ('ONLINE','SYSTEM');

select tablespace_name,extent_management,status from dba_tablespaces;


set lines 120
col r format a100
select 'alter database datafile '||chr(39)||file_name||chr(39)||' resize '||round(bytes/1024/1024,0)||' M;' r 
from dba_data_files f where tablespace_name in ( select tablespace_name
 from (
select x.*, round((mbf/mba) * 100,2) percentFree
from (select tablespace_name
,(select round(sum(bytes)/1024/1024,2) from dba_data_files f where f.TABLESPACE_NAME = tab.tablespace_name) MBA 
,(select round(sum(bytes)/1024/1024,2) from dba_free_space free where free.TABLESPACE_NAME = tab.tablespace_name) MBF
from dba_tablespaces tab ) x
) where percentfree < 25
 );


prompt effective space

set pages 0
select tablespace_name,replace(to_char(GB),',','.') GB from (
select tablespace_name,sum(round(bytes/1024/1024/1024,2)) GB from dba_segments 
group by tablespace_name
order by tablespace_name
);



prompt

prompt ########################################################################

prompt ###  Temporary Tablespaces

prompt ########################################################################

set lines 120
col value format A50
col parameter format a25
col temporary_tablespace format a20

select tablespace_name
,status,sum(bytes)/1024/1024 MB 
from dba_temp_files
group by tablespace_name,status;

set lines 120
col file_name format a60
col MB format 99999.00
col file_id   format 999

select file_id,tablespace_name,file_name
,bytes/1024/1024 MB
,maxbytes/1024/1024 MAXMB
,status 
from dba_temp_files order by tablespace_name,file_name;

prompt ########################################################################

prompt ###  Invalid Objects

prompt ########################################################################

col object_name format a40
select owner,object_type,object_name from dba_objects where status != 'VALID' order by 1,2;

prompt ########################################################################

prompt ###  Undo Tablespace and Sizing

prompt ########################################################################

col "UNDO RETENTION [Sec]" format A25

prompt Calculate UNDO_RETENTION for given UNDO Tablespace ...

SELECT d.undo_size/(1024*1024) "ACTUAL UNDO SIZE [MByte]",
       SUBSTR(e.value,1,25) "UNDO RETENTION [Sec]",
       ROUND((d.undo_size / (to_number(f.value) *
       g.undo_block_per_sec))) "OPTIMAL UNDO RETENTION [Sec]"
  FROM (
       SELECT SUM(a.bytes) undo_size
          FROM v$datafile a,
               v$tablespace b,
               dba_tablespaces c
         WHERE c.contents = 'UNDO'
           AND c.status = 'ONLINE'
           AND b.name = c.tablespace_name
           AND a.ts# = b.ts#
       ) d,
       v$parameter e,
       v$parameter f,
       (
       SELECT MAX(undoblks/((end_time-begin_time)*3600*24))
              undo_block_per_sec
         FROM v$undostat
       ) g
WHERE e.name = 'undo_retention'
  AND f.name = 'db_block_size'
/



prompt Calculate Needed Undo Size for given Database Activity

SELECT d.undo_size/(1024*1024) "ACTUAL UNDO SIZE [MByte]",
       SUBSTR(e.value,1,25) "UNDO RETENTION [Sec]",
       (TO_NUMBER(e.value) * TO_NUMBER(f.value) *
       g.undo_block_per_sec) / (1024*1024) 
      "NEEDED UNDO SIZE [MByte]"
  FROM (
       SELECT SUM(a.bytes) undo_size
         FROM v$datafile a,
              v$tablespace b,
              dba_tablespaces c
        WHERE c.contents = 'UNDO'
          AND c.status = 'ONLINE'
          AND b.name = c.tablespace_name
          AND a.ts# = b.ts#
       ) d,
      v$parameter e,
       v$parameter f,
       (
       SELECT MAX(undoblks/((end_time-begin_time)*3600*24))
         undo_block_per_sec
         FROM v$undostat
       ) g
 WHERE e.name = 'undo_retention'
  AND f.name = 'db_block_size'
/


prompt

prompt ########################################################################

prompt ###  Oracle Directories

prompt ########################################################################

col owner format a10
col directory_name format a25
col directory_path format a80
select owner,directory_name,directory_path from dba_directories;

prompt

prompt ########################################################################

prompt ###  Oracle Text Dependencies

prompt ########################################################################

select table_owner,index_type,count(1) 
from dba_indexes where 
    index_type not like 'IOT%'
and index_type not like 'NORMAL%'
and index_type not like 'LOB%'
and index_type not like 'FUNCTION-BASED NORMAL%'
and index_type not like 'CLUSTER%'
group by table_owner,index_type;

select table_owner,index_type,index_name 
from dba_indexes where 
    index_type not like 'IOT%'
and index_type not like 'NORMAL%'
and index_type not like 'LOB%'
and index_type not like 'FUNCTION-BASED NORMAL%'
and index_type not like 'CLUSTER%'
order by 1,2,3;


prompt

prompt ########################################################################

prompt ###  Performance

prompt ########################################################################


prompt pga information ...
column "name pgastat" format a40
column value format 999,999,999,999,999
 
select name "name pgastat",value
from
v$pgastat
order by
value desc;


prompt pga_target_advice ...
set lines 200
select * from v$pga_target_advice;


prompt You can also obtain information about workarea executions by querying the v$sysstat view shown here:
prompt Pga_aggregate_target "multipass" executions indicate a RAM shortage, and you should always allocate 
prompt enough RAM to ensure that at least 95 percent of connected tasks can acquire their RAM memory optimally.

col c1 heading 'Workarea|Profile' format a35
col c2 heading 'Count' format 999,999,999
col c3 heading 'Percentage' format 999
 
select name c1,cnt c2,decode(total, 0, 0, round(cnt*100/total)) c3
from
(
select name,value cnt,(sum(value) over ()) total
from
v$sysstat
where
name like 'workarea exec%'
);






Set lines 120
Set pages 30
 
Col wait_class format a30
 
select wait_class, sum(time_waited), sum(time_waited)/sum(total_waits) Sum_Waits
From v$system_wait_class
Group by wait_class
Order by 3 desc;
 
Select a.event, a.total_waits, a.time_waited, a.average_wait
From v$system_event a, v$event_name b, v$system_wait_class c
Where a.event_id=b.event_id
And b.wait_class#=c.wait_class#
And c.wait_class in ('Application','Concurrency')
order by average_wait desc;


set lines 120
col event format a30
col username format a15

prompt row lock contention 1 ...

select a.sid, a.event, a.total_waits, a.time_waited, a.average_wait, b.username
from v$session_event a, v$session b
where time_waited > 0
and a.sid=b.sid
and b.username is not NULL
and a.event='enq: TX - row lock contention';


prompt row lock contention 2 ...

col sql_text format a100
select
    sid,
    sql_text "sql_text row_lock contention"
from
    v$session s,
    v$sql q 
where
    sid in
    (select
       sid
    from
       v$session
   where
       state in ('WAITING')
   and
       wait_class != 'Idle'
   and 
       event='enq: TX - row lock contention'
   and 
      (q.sql_id = s.sql_id or q.sql_id = s.prev_sql_id));


-- sizing pga_aggregate_target ... http://www.dba-oracle.com/tips_pga_aggregate_target.htm

col c1 heading 'Workarea|Profile' format a35
col c2 heading 'Count' format 999,999,999
col c3 heading 'Percentage' format 999.9
 
select name c1,cnt c2,decode(total, 0, 0, round(cnt*100/total)) c3
from
(
select name,value cnt,(sum(value) over ()) total
from
v$sysstat
where
name like 'workarea exec%'
);


prompt

prompt ########################################################################

prompt ###  NLS Information

prompt ########################################################################


col parameter format a30
col value format a40

select * from NLS_DATABASE_PARAMETERS
/





prompt

prompt ########################################################################

prompt ###  User Information

prompt ########################################################################

set pages 500
set lines 150
col username format a20
col profile format a20
col lock_date format a14

select username
,default_tablespace
,temporary_tablespace
,account_status
,lock_date
,profile
from dba_users
order by lock_date desc nulls first
/

set pages 50

prompt

prompt ########################################################################

prompt ###  Archiving Information

prompt ########################################################################

set pages 500

select to_char(first_time,'yyyy.mm.dd hh24') first_time
,count(1) 
from v$archived_log 
where first_time > sysdate - 90
group by to_char(first_time,'yyyy.mm.dd hh24') 
order by 1 desc;

set pages 50


prompt

prompt ########################################################################

prompt ###  Database Triggers

prompt ########################################################################

col owner format a15

select owner,trigger_type,count(1) from dba_triggers group by owner,trigger_type order by owner,trigger_type;

select owner,trigger_type,count(1) from dba_triggers 
where trigger_type like 'AFTER EVENT'
group by owner,trigger_type 
order by owner,trigger_type;

prompt

prompt ########################################################################

prompt ###  ROLEs

prompt ########################################################################
column role format a30
column password_required format a10 heading 'Pass'
column authentication_type format a11 heading 'Authentication Type'

select * from dba_roles order by role;


prompt

prompt ########################################################################

prompt ###  ROLEs

prompt ########################################################################
col grantee format a30
col privilege format a40
select * from dba_sys_privs order by grantee,privilege;


prompt

prompt ########################################################################

prompt ###  role privileges

prompt ########################################################################

col granted_role format a30
col admin_option format a10
col default_role format a10

select * from dba_role_privs order by grantee,granted_role;


prompt

prompt ########################################################################

prompt ###  Segment Information

prompt ########################################################################

set pages 100

select owner,
round(sum(bytes)/1024/1024/1024,2) GB from dba_segments
where owner not in ('SYS','SYSTEM','OUTLN','WMSYS','DBSNMP','EXFSYS','XDB','ORDSYS','ORDDATA','MDSYS','SYSMAN','FLOWS_FILES')
group by owner 
order by 2 desc;


prompt for quota information  ...

select owner,tablespace_name
,round(sum(bytes)/1024/1024/1024,2) GB from dba_segments
where owner not in ('SYS','SYSTEM','OUTLN','WMSYS','DBSNMP','EXFSYS','XDB','ORDSYS','ORDDATA','MDSYS','SYSMAN','FLOWS_FILES')
group by owner , tablespace_name
order by 1,2 desc;



select owner, count(1) cnt
from dba_segments group by owner order by 2 desc,1;


select owner,segment_type, count(1) cnt
from dba_segments group by owner,segment_type order by 1,2 desc;


select sum(bytes)/1024/1024/1024 GBDBASegMents from dba_segments;


prompt ########################################################################

prompt ###  Segment Information Detail by Size

prompt ########################################################################

col segment_name format a55
col owner format a20
set lines 180

select * from (
select owner,segment_name,segment_type,tablespace_name
,rank() over (partition by owner order by bytes desc) rnk
,round(bytes/1024/1024/1024,2) GB from dba_segments
where owner not in ('SYS','SYSTEM','OUTLN','WMSYS','DBSNMP','EXFSYS','XDB','ORDSYS','ORDDATA','MDSYS','SYSMAN','FLOWS_FILES')
and owner not like 'APEX%') x where x.rnk<=10
order by owner,rnk asc;

prompt ########################################################################

prompt ###  Segment Information Detail by Name

prompt ########################################################################


col segment_name format a55
col owner format a20
col tablespace_name format a30
set lines 180

select * from (
select owner,segment_name,segment_type,tablespace_name
,rank() over (partition by owner order by bytes desc) rnk
,round(bytes/1024/1024/1024,2) GB from dba_segments
where owner not in ('SYS','SYSTEM','OUTLN','WMSYS','DBSNMP','EXFSYS','XDB','ORDSYS','ORDDATA','MDSYS','SYSMAN','FLOWS_FILES')
and owner not like 'APEX%') x where x.rnk<=10
order by owner,segment_type,segment_name asc;

prompt

prompt ########################################################################

prompt ###  Oracle Parameters

prompt ########################################################################

set lines 120
set pages 100
col name format a30
col value format a60
select  '&1' DB, name,value,isdefault 
from v$parameter where name in ('background_dump_dest'
                               ,'db_cache_size'
                               ,'pga_aggregate_target'
                               ,'sga_target'
                               ,'db_block_size'
                               ,'db_block_buffers'
                               ,'db_cache_size'
                               ,'log_buffer'
                               ,'sga_target'
                               ,'sga_max_size'
                               ,'cpu_count'
                               ,'open_cursors'
                               ,'processes'
                               ,'sort_area_size'
                               ,'hash_area_size'
                               ,'db_block_checking'
                               ,'result_cache_max_size'
                               ,'shared_pool_size'
) order 
by decode(name                 ,'sga_target'                ,'111'
                               ,'sga_max_size'              ,'112'
                               ,'memory_target'             ,'113'
                               ,'memory_max_target'         ,'114'
                               ,'db_cache_size'             ,'115'
                               ,'db_block_buffers'          ,'116'
                               ,'result_cache_max_size'     ,'117'
                               ,'pga_aggregate_target'      ,'118'
                               ,'log_buffer'                ,'119'
                               ,'shared_pool_size'          ,'120'
                               ,'sort_area_size'            ,'121'
                               ,'hash_area_size'            ,'122'
                               ,'db_block_size'             ,'123'
                               ,'background_dump_dest'      ,'124'
                               ,'db_block_checking'         ,'125'
                               ,'open_cursors'              ,'125'
                               ,'processes'                 ,'127'
                               ,'cpu_count'                 ,'128'
                               ,'199');



prompt

prompt ########################################################################

prompt ###  Oracle Parameters (all)

prompt ########################################################################



set pages 100
col name format a40
select isdefault,name,value from v$parameter order by 1,2,3;


 
 
set lines 120


prompt

prompt ########################################################################

prompt ###  Session Information

prompt ########################################################################

set lines 250
col sid format 99999
col opname format a30
col message format a30
col target format a30
col machine format a30
col program format a30
col event format a30
col client_info format a20
col module format a10
col action format a10
col blocking_session format a10 heading 'BS'

select 
 sid
,serial#
,username,status,PROCESS,machine
,program
,sql_id
,MODULE
,action
,CLIENT_INFO
,EVENT
,lockwait
,BLOCKING_SESSION
from v$session;

prompt

prompt ########################################################################

prompt ###  Condensed Session Source Information

prompt ########################################################################

set lines 120
set pages 0
col machine format a20
col program format a20
col username format a20

select username,machine,program,port
from v$session
where program not like 'ORACLE.EXE%'
and program not like 'OMS%'
and program not like 'emagent%'
order by 1,2;

select username,machine,program,count(1)
from v$session
where program not like 'ORACLE.EXE%'
and program not like 'OMS%'
and program not like 'emagent%'
group by username,machine,program
order by 1,2;

prompt ########################################################################

prompt ###  Session Information (LongOps)

prompt ########################################################################


col units format a15
col time_elapsed format 99999 heading te
col TIME_REMAINING format 99999 heading tr
col ELAPSED_SECONDS format 99999 heading es
col opname format a35

select
  SID                  
 ,SERIAL#              
 ,OPNAME               
 ,TARGET               
 ,TARGET_DESC          
 ,SOFAR                
 ,TOTALWORK            
 ,UNITS                
 ,START_TIME           
 ,LAST_UPDATE_TIME     
 ,TIME_REMAINING       
 ,ELAPSED_SECONDS      
 ,CONTEXT              
 ,MESSAGE              
 ,USERNAME             
from v$session_longops
order by ELAPSED_SECONDS desc;

prompt

prompt ########################################################################

prompt ###  Policies defined in the Database

prompt ########################################################################

select object_owner,object_name,policy_name,enable from dba_policies
order by object_name,policy_name;

prompt ########################################################################

prompt ###  Buffer Pool Details

prompt ########################################################################


prompt
prompt candidates for full table scans ...
prompt
 SELECT 
     object_owner,
     object_name ,COUNT(*) FTS_NO
   FROM
 dba_hist_sql_plan
   WHERE
   object_owner NOT LIKE '%SYS%' AND
      operation = 'TABLE ACCESS'
      AND TRUNC(TIMESTAMP)=TRUNC(SYSDATE)-1 AND
      options = 'FULL' GROUP BY object_name,object_owner ORDER BY 3 DESC;

prompt 
prompt sqlids with full tablescans of today
prompt
SELECT DISTINCT
    sql_id stid,
     object_owner owner,
     object_name NAME
   FROM
 dba_hist_sql_plan
   WHERE
   object_owner NOT LIKE '%SYS%' AND
      operation = 'TABLE ACCESS'
      AND TRUNC(TIMESTAMP)=TRUNC(SYSDATE)-1 AND
      options = 'FULL' ORDER BY owner,sql_id;
      
-- SELECT sql_text FROM v$sqltext WHERE sql_id='30kjjsb9dn0uz' ORDER BY piece



prompt
prompt show buffer cache usage per object ...
prompt

set lines 120
COLUMN OBJECT_NAME FORMAT A40
column object_type format a20
column owner       format a15
COLUMN NUMBER_OF_BLOCKS FORMAT 999,999,999,999

SELECT o.owner,o.OBJECT_NAME, o.object_type
   ,COUNT(*) NUMBER_OF_BLOCKS
     FROM DBA_OBJECTS o, V$BH bh
    WHERE o.DATA_OBJECT_ID = bh.OBJD
      AND o.OWNER         != 'SYS'
    GROUP BY o.owner,o.OBJECT_NAME,o.object_type
    ORDER BY COUNT(*);

prompt
prompt show pool usage ....
prompt

show parameter db_block_size

col sumbuf format 999,999,999

select x.name 
       ,x.block_size
       ,x.sumbuf
       ,round((x.block_size * x.sumbuf) /1024 /1024,2) MBBUF
from
(
SELECT NAME, BLOCK_SIZE, SUM(BUFFERS) sumbuf
  FROM V$BUFFER_POOL
 GROUP BY NAME, BLOCK_SIZE
 HAVING SUM(BUFFERS) > 0
 ) x;

spool off

