



function wegaAnalyzeLogFiles($what)
{
write-host "#########################################################"
write-host $what $alert.length

$idx1=$alert | where {$_ -match "ORA-" } | foreach-object {$_.ReadCount}
$idx2=$alert | where {$_ -match "RMAN-"} | foreach-object {$_.ReadCount}

for ($t=0; $t -lt $alert.length; $t++){
 foreach ($id in $idx1) {
   if ($id -eq $t) {
     write-host "#######"
     $alert[$t - 2]
     $alert[$t - 1]
	 $alert[$t]
	 $alert[$t + 1]
	 write-host
   }
 }
} 
for ($i=$alert.length - 10; $i -lt $alert.length; $i++){
  write-host "LAST LINES : " $alert[$i]
}
}



$hpcs_alert  = gc \\so-s2033\e$\oracle\admin\hpcs\bdump\alert_hpcs.log
$hpcs_rman_0 = gc \\so-s2033\f$\BackupOracle\hpcs_rman_level_0.log
#$hpcs_rman_d = gc \\so-s2033\f$\BackupOracle\rman_level_1_diff.log
$hpcs_rman_a = gc \\so-s2033\f$\BackupOracle\hpcs_rman_archivelog.log
$hpcs_array = @($hpcs_alert,$hpcs_rman_0,$hpcs_rman_a)
$hpcs_desc  = @("Alert Log File","RMAN Level 0","RMAN Archivelog")
for ($i=0; $i -lt $hpcs_array.length; $i++) {
  $alert      = $hpcs_array[$i]
  $alert_desc = $hpcs_desc[$i]
  wegaAnalyzeLogFiles("## SO-S2033 HPCS " + $alert_desc)
}

Clear-Variable hpcs_* -Scope Global

$tainstdb_alert= gc \\so-s2033\e$\oracle\admin\tainstdb\bdump\alert_tainstdb.log
$tainstdb_rman_0 = gc \\so-s2033\f$\BackupOracle\tainstdb_rman_level_0.log
$tainstdb_rman_a = gc \\so-s2033\f$\BackupOracle\tainstdb_rman_archivelog.log
$tainstdb_array = @($tainstdb_alert,$tainstdb_rman_0,$tainstdb_rman_a)
$tainstdb_desc  = @("Alert Log File","RMAN Level 0","RMAN Archivelog")
for ($i=0; $i -lt $tainstdb_array.length; $i++) {
  $alert = $tainstdb_array[$i]
  $alert_desc = $tainstdb_desc[$i]
  wegaAnalyzeLogFiles("## SO-S2033 TAINSTDB " + $alert_desc)
}

Clear-Variable tainstdb_* -Scope Global


$symyx_alert = gc \\so-s2010\e$\oracle\admin\symyx\bdump\alert_symyx.log
$symyx_rman_0 = gc \\so-s2010\e$\BackupOracle\rman_level_0.log
$symyx_rman_a = gc \\so-s2010\e$\BackupOracle\rman_archivelog.log
$symyx_array = @($symyx_alert,$symyx_rman_0,$symyx_rman_a)
$symyx_desc  = @("Alert Log File","RMAN Level 0","RMAN Archivelog")
for ($i=0; $i -lt $symyx_array.length; $i++) {
  $alert = $symyx_array[$i]
  $alert_desc = $symyx_desc[$i]
  wegaAnalyzeLogFiles("## SO-S2010 SYMYX " + $alert_desc)
}

$enahyd_alert = gc \\so-s2010\e$\oracle\admin\Enahyd9\bdump\alert_enahyd9.log
$enahyd_rman_0 = gc \\so-s2010\e$\BackupOracle\rman_level_0_enahyd.log
$enahyd_rman_a = gc \\so-s2010\e$\BackupOracle\rman_archivelog_enahyd.log
$enahyd_array = @($enahyd_alert,$enahyd_rman_0,$enahyd_rman_a)
$enahyd_desc  = @("Alert Log File","RMAN Level 0","RMAN Archivelog")
for ($i=0; $i -lt $enahyd_array.length; $i++) {
  $alert = $enahyd_array[$i]
  $alert_desc = $enahyd_desc[$i]
  wegaAnalyzeLogFiles("## SO-S2010 ENAHYD " + $alert_desc)
}


$elnp_alert  = gc \\so-s3030\c$\oracle\diag\rdbms\elnp11\elnp11\trace\alert_elnp11.log
$elnp_rman_0 = gc \\so-s3030\f$\BackupOracle\rman_elnp11_level_0.log
$elnp_rman_1 = gc \\so-s3030\f$\BackupOracle\rman_elnp11_level_1.log
$elnp_array = @($elnp_alert,$elnp_rman_0,$elnp_rman_1)
$elnp_desc  = @("Alert Log File","RMAN Level 0","RMAN Archivelog")
for ($i=0; $i -lt $elnp_array.length; $i++) {
  $alert = $elnp_array[$i]
  $alert_desc = $elnp_desc[$i]
  wegaAnalyzeLogFiles("## SO-S3030 ELNP " + $alert_desc)
}



###############################################################################
# Manual File Discovery
#
vi \\so-s3030\c$\oracle\diag\rdbms\elnp11\elnp11\trace\alert_elnp11.log
vi \\so-s2010\e$\oracle\admin\Enahyd9\bdump\alert_enahyd9.log
vi \\so-s2010\e$\oracle\admin\symyx\bdump\alert_symyx.log
vi \\so-s2033\e$\oracle\admin\tainstdb\bdump\alert_tainstdb.log
vi \\so-s2033\e$\oracle\admin\hpcs\bdump\alert_hpcs.log

vi \\so-s3030\f$\BackupOracle\rman_elnp_level_0.log
vi \\so-s2010\e$\BackupOracle\rman_level_0_enahyd.log
vi \\so-s2010\e$\BackupOracle\rman_level_0.log
vi \\so-s2033\f$\BackupOracle\tainstdb_rman_level_0.log
vi \\so-s2033\f$\BackupOracle\hpcs_rman_level_0.log


alter database tablespace SYMYXLOB add datafile 'E:\ORACLE\ORADATA\SYMYX\SYMYXLOB17.DBF' size 4000M autoextend off;



function create-7zip([String] $aDirectory, [String] $aZipfile){
    [string]$pathToZipExe = "C:\Program Files\7-zip\7z.exe";
    [Array]$arguments = "a", "-tzip", "$aZipfile", "$aDirectory", "-r";
    & $pathToZipExe $arguments;
}


create-7zip "c:\temp\myFolder" "c:\temp\myFolder.zip"

copy-item \\so-s2033\f$\BackupOracle\hpcs_rman_level_0.log \\so-s2033\f$\BackupOracle\_save20130226
clear-content \\so-s2033\f$\BackupOracle\hpcs_rman_level_0.log