--connect sys/solvias@wega_SO-S2030_ELNP.SOLVIAS.CH as sysdba
--spool so-s2030_elnp.log
--@check ELNP
--spool off

connect system/xxx@SO-S3130_ELNT11.SOLVIAS.CH
spool so-s3130_elnt.log
@check ELNT
spool off


connect sys/xxx@wega_SO-S3030_ELNP11.SOLVIAS.CH as sysdba
spool so-s3030_elnp.log
@check ELNP
spool off


connect system/xxx@wega_enahyd9.solvias.ch
spool so-s2010_enahyd.log
@check ENAYHD9
spool off


connect system/xxx@wega_SO-S2010_SYMYX.SOLVIAS.CH
spool so-s2010_symyx.log
@check SYMYX
spool off


connect sys/xxx@SO-S2033_HPCS.SOLVIAS.CH as sysdba
spool so-s2033_hpcs.log
@check HPCS
spool off


connect sys/xxx@SO-S2033_tainstdb.SOLVIAS.CH as sysdba
spool so-s2033_tainstdb.log
@check TAINSTDB
spool off




exit
