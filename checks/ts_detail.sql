
connect sys/xxxx@wega_SO-S3030_ELNP11.SOLVIAS.CH as sysdba
--connect system/xxx@wega_SO-S2010_SYMYX.SOLVIAS.CH
--connect sys/xxx@SO-S2033_tainstdb.SOLVIAS.CH as sysdba

show user
select * from global_name;

set lines 200
col file_name format a50
col alterStmt format a80
col tablespace_name format a25
col percent format a7 heading '%'


ACCEPT myTS CHAR PROMPT 'Tablespace: ';

select tablespace_name
,sum(bytes)/1024/1024/1024 "total space in GB"
from dba_segments
where tablespace_name = ('&&myTS')
group by tablespace_name;

select tablespace_name
,sum(bytes)/1024/1024/1024 "free space in GB"
from dba_free_space
where tablespace_name = ('&&myTS')
group by tablespace_name;


select tablespace_name
, file_id
, round(bytes/1024/1024/1024,2) GB
, round(maxbytes/1024/1024/1024,2) GBMax
, round(bytes/maxbytes,3) * 100 || '%' percent
, autoextensible
,'alter database datafile ' || '''' || file_name || '''' || ' resize xxM;'   alterStmt
from dba_data_files
where tablespace_name = ('&&myTS')
and autoextensible='YES'
order by tablespace_name,file_name;


select  file_name from dba_data_files where tablespace_name = ('&&myTS')  order by 1 asc;

