--------------------------------------------------  
-- Wega Informatik AG  
-- Purpose : TAINSTDB Change Init Parameters
-- $Date$  
-- $Author$  
-- $HeadURL$  
-- $Revision$  
-------------------------------------------------- 

-- HPCS Parameter Change

alter system set processes=300 scope=spfile;
alter system set sessions=300 scope=spfile;
alter system set control_file_record_keep_time=30 scope=both;


