--------------------------------------------------  
-- Wega Informatik AG  
-- Purpose : TAInstDB Enable Archiving
-- $Date$  
-- $Author$  
-- $HeadURL$  
-- $Revision$  
-------------------------------------------------- 
###############################################################################
# prepare Archiving
mkdir f:\archive\tainstdb

###############################################################################
# enable Archiving
col host_name format a30
SELECT LOG_MODE FROM SYS.V$DATABASE;
select instance_name,host_name from v$instance;
v$archived_log

startup mount;
alter database archivelog;
alter system set log_archive_start=TRUE                     scope=spfile;
alter system set log_archive_dest='f:\archive\tainstdb'     scope=spfile;
alter system set log_archive_format='tainstdb_%t-%S.arc'    scope=spfile;
shutdown immediate;
startup;
alter system switch logfile;


###############################################################################
rman connect target /
CONFIGURE RETENTION POLICY TO RECOVERY WINDOW OF 14 DAYS;
CONFIGURE BACKUP OPTIMIZATION OFF; # default
CONFIGURE DEFAULT DEVICE TYPE TO DISK; # default
CONFIGURE CONTROLFILE AUTOBACKUP ON;
CONFIGURE CONTROLFILE AUTOBACKUP FORMAT FOR DEVICE TYPE DISK TO '\\SO-f0002\dbbackup$\S2033\tainstdb\cf%F_tainstdb';
CONFIGURE DEVICE TYPE DISK PARALLELISM 1; # default
CONFIGURE DATAFILE BACKUP COPIES FOR DEVICE TYPE DISK TO 1; # default
CONFIGURE ARCHIVELOG BACKUP COPIES FOR DEVICE TYPE DISK TO 1; # default
CONFIGURE MAXSETSIZE TO 4147483648;
CONFIGURE SNAPSHOT CONTROLFILE NAME TO '\\SO-F0002\DBBACKUP$\S2033\TAINSTDB\SNAPCF_TAINSTDB.F';
###############################################################################


archive log list
show parameter log_archive_dest
show parameter log_archive_format
show parameter log_archive_start

rman
connect target user/pass
show all


archive log list
show parameter log_archive_dest
show parameter log_archive_format
show parameter log_archive_start

rman
connect target user/pass
show all

#################################################################################
# TAINSTDB

Datenbank-Log-Modus              Archive-Modus
Automatische Archivierung             Aktiviert
Archivierungsziel            E:\oracle\onlinebackup\tainstdb
-lteste Online-Log-Sequenz     23221
N�chste zu archivierende Log-Sequenz   23224
Aktuelle Log-Sequenz           23224


NAME                                 TYPE        VALUE
------------------------------------ ----------- ------------------------------
log_archive_dest                     string
log_archive_dest_state_1             string      enable
log_archive_dest_state_10            string      enable
log_archive_dest_state_2             string      enable
log_archive_dest_state_3             string      enable
log_archive_dest_state_4             string      enable
log_archive_dest_state_5             string      enable
log_archive_dest_state_6             string      enable
log_archive_dest_state_7             string      enable
log_archive_dest_state_8             string      enable
log_archive_dest_state_9             string      enable
log_archive_dest_1                   string      LOCATION=E:\oracle\onlinebacku
                                                 p\tainstdb
log_archive_dest_10                  string
log_archive_dest_2                   string
log_archive_dest_3                   string
log_archive_dest_4                   string
log_archive_dest_5                   string
log_archive_dest_6                   string
log_archive_dest_7                   string
log_archive_dest_8                   string
log_archive_dest_9                   string

NAME                                 TYPE        VALUE
------------------------------------ ----------- ------------------------------
log_archive_format                   string      %t_%s.dbf

NAME                                 TYPE        VALUE
------------------------------------ ----------- ------------------------------
log_archive_start                    boolean     TRUE


Mit Ziel-Datenbank verbunden: TAINSTDB (DBID=2865861197)

RMAN> show all;

Kontrolldatei der Zieldatenbank wird anstelle des Recovery-Katalogs verwendet
RMAN-Konfigurationsparameter sind:
CONFIGURE RETENTION POLICY TO RECOVERY WINDOW OF 14 DAYS;
CONFIGURE BACKUP OPTIMIZATION OFF; # default
CONFIGURE DEFAULT DEVICE TYPE TO DISK; # default
CONFIGURE CONTROLFILE AUTOBACKUP ON;
CONFIGURE CONTROLFILE AUTOBACKUP FORMAT FOR DEVICE TYPE DISK TO '\\SO-f0002\dbbackup$\S2033\tainstdb\cf%F_tainstdb';
CONFIGURE DEVICE TYPE DISK PARALLELISM 1; # default
CONFIGURE DATAFILE BACKUP COPIES FOR DEVICE TYPE DISK TO 1; # default
CONFIGURE ARCHIVELOG BACKUP COPIES FOR DEVICE TYPE DISK TO 1; # default
CONFIGURE MAXSETSIZE TO 4147483648;
CONFIGURE SNAPSHOT CONTROLFILE NAME TO '\\SO-F0002\DBBACKUP$\S2033\TAINSTDB\SNAPCF_TAINSTDB.F';