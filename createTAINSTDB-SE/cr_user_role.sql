--------------------------------------------------  
-- Wega Informatik AG  
-- Purpose : TAInstDB Create Users and Privileges
-- $Date$  
-- $Author$  
-- $HeadURL$  
-- $Revision$  
-------------------------------------------------- 
select * from v$instance;

drop user tama cascade;
drop user tauser cascade;
drop user tasys  cascade;
drop user ta_fk  cascade;

CREATE USER "TAMA" IDENTIFIED BY VALUES 'EA72F9D1D35CB328'
DEFAULT TABLESPACE "TA_FK"
TEMPORARY TABLESPACE "TEMP";


GRANT CREATE VIEW TO "TAMA";
GRANT ALTER SESSION TO "TAMA";
GRANT CREATE SESSION TO "TAMA";


CREATE USER "TAUSER" IDENTIFIED BY VALUES 'E86E8EAB61A399FD'  
DEFAULT TABLESPACE "TA_FK"   
TEMPORARY TABLESPACE "TEMP";

GRANT CREATE VIEW TO "TAUSER";
GRANT ALTER SESSION TO "TAUSER";
GRANT CREATE SESSION TO "TAUSER";


CREATE USER "TASYS" IDENTIFIED BY VALUES '20FBF95E1BFFF6A9'
      TEMPORARY TABLESPACE "TEMP";


CREATE USER "TA_FK" IDENTIFIED BY VALUES '5BFCA54092766DF1'
      DEFAULT TABLESPACE "TA_FK"
      TEMPORARY TABLESPACE "TMPTA_FK";

grant unlimited tablespace to ta_fk;	 
grant unlimited tablespace to tasys; 

-- select 'grant '||privilege||' to '||grantee||';' from dba_sys_privs where grantee like 'T%' order by grantee;	  
grant DROP USER to TA_FK;
grant CREATE VIEW to TA_FK with admin option;
grant CREATE SESSION to TA_FK;
grant CREATE TABLE to TA_FK with admin option;
grant DROP ANY INDEX to TA_FK;
grant DROP TABLESPACE to TA_FK;
grant DROP ANY TRIGGER to TA_FK;
grant ALTER ANY TRIGGER to TA_FK;
grant MANAGE TABLESPACE to TA_FK;
grant SELECT ANY DICTIONARY to TA_FK;
grant CREATE ANY PROCEDURE to TA_FK;
grant GRANT ANY PRIVILEGE to TA_FK;
grant CREATE ANY SEQUENCE to TA_FK;
grant ALTER ANY PROCEDURE to TA_FK;
grant DROP ANY PROCEDURE to TA_FK;
grant CREATE ANY TRIGGER to TA_FK;
grant DROP ANY SEQUENCE to TA_FK;
grant SELECT ANY TABLE to TA_FK;
grant CREATE ANY INDEX to TA_FK;
grant DROP ANY TABLE to TA_FK;
grant CREATE VIEW to TAMA;
grant ALTER SESSION to TAMA;
grant CREATE SESSION to TAMA;
grant CREATE VIEW to TASYS;
grant CREATE TABLE to TASYS;
grant ALTER SESSION to TASYS;
grant DROP ANY VIEW to TASYS;
grant CREATE SESSION to TASYS;
grant CREATE ANY VIEW to TASYS;
grant CREATE SEQUENCE to TASYS;
grant SELECT ANY TABLE to TASYS;
grant CREATE VIEW to TAUSER;
grant ALTER SESSION to TAUSER;
grant CREATE SESSION to TAUSER;
	  
select * from dba_sys_privs where grantee like 'T%' order by grantee;	  
TA_FK                          DROP USER                                NO
TA_FK                          CREATE VIEW                              YES
TA_FK                          CREATE SESSION                           NO
TA_FK                          CREATE TABLE                             YES
TA_FK                          DROP ANY INDEX                           NO
TA_FK                          DROP TABLESPACE                          NO
TA_FK                          DROP ANY TRIGGER                         NO
TA_FK                          ALTER ANY TRIGGER                        NO
TA_FK                          MANAGE TABLESPACE                        NO
TA_FK                          SELECT ANY DICTIONARY                    NO
TA_FK                          CREATE ANY PROCEDURE                     NO
TA_FK                          GRANT ANY PRIVILEGE                      NO
TA_FK                          CREATE ANY SEQUENCE                      NO
TA_FK                          ALTER ANY PROCEDURE                      NO
TA_FK                          DROP ANY PROCEDURE                       NO
TA_FK                          CREATE ANY TRIGGER                       NO
TA_FK                          DROP ANY SEQUENCE                        NO
TA_FK                          SELECT ANY TABLE                         NO
TA_FK                          CREATE ANY INDEX                         NO
TA_FK                          DROP ANY TABLE                           NO
TAMA                           CREATE VIEW                              NO
TAMA                           ALTER SESSION                            NO
TAMA                           CREATE SESSION                           NO
TASYS                          CREATE VIEW                              NO
TASYS                          CREATE TABLE                             NO
TASYS                          ALTER SESSION                            NO
TASYS                          DROP ANY VIEW                            NO
TASYS                          CREATE SESSION                           NO
TASYS                          CREATE ANY VIEW                          NO
TASYS                          CREATE SEQUENCE                          NO
TASYS                          SELECT ANY TABLE                         NO
TAUSER                         CREATE VIEW                              NO
TAUSER                         ALTER SESSION                            NO
TAUSER                         CREATE SESSION                           NO

SQL> select * from dba_role_privs where grantee like 'T%';

ORA-01950: no privileges on tablespace 'TA_FK'
ORA-01950: no privileges on tablespace 'SYSTEM'


. importing TASYS's objects into TASYS
IMP-00017: following statement failed with ORACLE error 1950:
 "CREATE TABLE "ARCHIVE" ("ARCHIVE_ID" NUMBER(*,0), "DB_NAME" NVARCHAR2(30) N"
 "OT NULL ENABLE, "ARCHIVE_DATE" DATE NOT NULL ENABLE, "START_DATE" DATE NOT "
 "NULL ENABLE, "END_DATE" DATE NOT NULL ENABLE, "MAX_SIZE_MB" NUMBER(18, 7) N"
 "OT NULL ENABLE, "ACTUAL_SIZE_MB" NUMBER(18, 7) NOT NULL ENABLE, "TARGET_PAT"
 "H" NVARCHAR2(255) NOT NULL ENABLE, "MEDIA_TYPE" NVARCHAR2(15) NOT NULL ENAB"
 "LE, "MEDIA_IDENTIFIER" NVARCHAR2(128) NOT NULL ENABLE, "COMMENTS" NVARCHAR2"
 "(255) NOT NULL ENABLE, "SCRIPT" NCLOB NOT NULL ENABLE, "ARCHIVE_NAME" NVARC"
 "HAR2(128) NOT NULL ENABLE, "FILENAME" NVARCHAR2(64) NOT NULL ENABLE, "DBVER"
 "SION" NVARCHAR2(128) NOT NULL ENABLE, "PURGE_DATE" DATE, "PURGE_BY" NVARCHA"
 "R2(50), "ARCHIVE_BY" NVARCHAR2(50) NOT NULL ENABLE)  PCTFREE 10 PCTUSED 40 "
 "INITRANS 1 MAXTRANS 255 STORAGE(INITIAL 65536 FREELISTS 1 FREELIST GROUPS 1"
 ") TABLESPACE "SYSTEM" LOGGING NOCOMPRESS LOB ("SCRIPT") STORE AS  (TABLESPA"
 "CE "SYSTEM" ENABLE STORAGE IN ROW CHUNK 8192 PCTVERSION 10 NOCACHE  STORAGE"
 "(INITIAL 65536 FREELISTS 1 FREELIST GROUPS 1))"
IMP-00003: ORACLE error 1950 encountered
ORA-01950: no privileges on tablespace 'SYSTEM'
. importing TA_FK's objects into TA_FK

IMP-00017: following statement failed with ORACLE error 1950:
 "CREATE TABLE "ARCHIVE" ("ARCHIVE_ID" NUMBER(*,0), "DB_NAME" NVARCHAR2(30) N"
 "OT NULL ENABLE, "ARCHIVE_DATE" DATE NOT NULL ENABLE, "START_DATE" DATE NOT "
 "NULL ENABLE, "END_DATE" DATE NOT NULL ENABLE, "MAX_SIZE_MB" NUMBER(18, 7) N"
 "OT NULL ENABLE, "ACTUAL_SIZE_MB" NUMBER(18, 7) NOT NULL ENABLE, "TARGET_PAT"
 "H" NVARCHAR2(255) NOT NULL ENABLE, "MEDIA_TYPE" NVARCHAR2(15) NOT NULL ENAB"
 "LE, "MEDIA_IDENTIFIER" NVARCHAR2(128) NOT NULL ENABLE, "COMMENTS" NVARCHAR2"
 "(255) NOT NULL ENABLE, "SCRIPT" NCLOB NOT NULL ENABLE, "ARCHIVE_NAME" NVARC"
 "HAR2(128) NOT NULL ENABLE, "FILENAME" NVARCHAR2(64) NOT NULL ENABLE, "DBVER"
 "SION" NVARCHAR2(128) NOT NULL ENABLE, "PURGE_DATE" DATE, "PURGE_BY" NVARCHA"
 "R2(50), "ARCHIVE_BY" NVARCHAR2(50) NOT NULL ENABLE)  PCTFREE 10 PCTUSED 40 "
 "INITRANS 1 MAXTRANS 255 STORAGE(INITIAL 65536 FREELISTS 1 FREELIST GROUPS 1"
 ") TABLESPACE "SYSTEM" LOGGING NOCOMPRESS LOB ("SCRIPT") STORE AS  (TABLESPA"
 "CE "SYSTEM" ENABLE STORAGE IN ROW CHUNK 8192 PCTVERSION 10 NOCACHE  STORAGE"
 "(INITIAL 65536 FREELISTS 1 FREELIST GROUPS 1))"
IMP-00003: ORACLE error 1950 encountered
ORA-01950: no privileges on tablespace 'SYSTEM'
. importing TA_FK's objects into TA_FK	  