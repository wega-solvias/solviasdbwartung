--------------------------------------------------  
-- Wega Informatik AG  
-- Purpose : TAInstDB Create Tablespaces
-- $Date$  
-- $Author$  
-- $HeadURL$  
-- $Revision$  
-------------------------------------------------- 

-- UNDOTBS1 -  500 M
-- INDX     -   25 M
-- SYSTEM   -  500 M
-- TOOLS    -   10 M
-- USERS    -   25 M
-- XDB      -  100 M

--SYSTEM                                500
alter database datafile 'G:\ORACLE\TAINSTDB\TAINSTDB_SYSTEM01.DBF' resize 500M;

-- UNDO
alter database datafile 'G:\ORACLE\TAINSTDB\TAINSTDB_UNDOTBS01.DBF' resize 500M;

--XDB                                   100
alter database datafile 'G:\ORACLE\TAINSTDB\TAINSTDB_XDB01.DBF' resize 100M;

-- TA_FK                                2560
create tablespace TA_FK     datafile 'G:\ORACLE\TAINSTDB\TAINSTDB_TA_FK_01.DBF' size 2048M AUTOEXTEND OFF LOGGING ONLINE PERMANENT EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;
alter  tablespace TA_FK add datafile 'G:\ORACLE\TAINSTDB\TAINSTDB_TA_FK_02.DBF' size 1024M autoextend off;


alter database tempfile 'G:\ORACLE\TAINSTDB\TAINSTDB_TEMP01.DBF' resize 500M;

CREATE TEMPORARY TABLESPACE TMPTA_FK 
      TEMPFILE 'G:\ORACLE\TAINSTDB\TAINSTDB_TMPTA_FK01.DBF' SIZE 500M
      EXTENT MANAGEMENT LOCAL UNIFORM SIZE 16M;

select tablespace_name,sum(bytes)/1024/1024 GB from 
dba_data_files
group by tablespace_name;

select tablespace_name
, sum(bytes)/1024/1024 GB
from dba_temp_files
group by tablespace_name;

