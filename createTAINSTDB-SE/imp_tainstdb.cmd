--------------------------------------------------  
-- Wega Informatik AG  
-- Purpose : Import Database TAINSTDB
-- $Date$  
-- $Author$  
-- $HeadURL$  
-- $Revision$  
-------------------------------------------------- 
exit

set ORACLE_SID=TAINSTDB
set NLS_LANG=AMERICAN_AMERICA.AL32UTF8


echo "starting full database from to import ..." >>  imp_TAINSTDB.log
date /t >> imp_TAINSTDB.log
time /t >> imp_TAINSTDB.log

imp userid=system file=exp_tainstdb_full_2012-11-15.dmp log=imp_tainstdb_full_2012-11-15.log fromuser=TAMA,TAUSER,TASYS,TA_FK touser=TAMA,TAUSER,TASYS,TA_FK


echo "full database from to import finished." >> imp_TAINSTDB.log
date /t >> imp_TAINSTDB.log
time /t >> imp_TAINSTDB.log


pause



