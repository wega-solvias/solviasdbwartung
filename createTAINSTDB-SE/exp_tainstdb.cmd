--------------------------------------------------  
-- Wega Informatik AG  
-- Purpose : Export Database Tainstdb
-- $Date$  
-- $Author$  
-- $HeadURL$  
-- $Revision$  
-------------------------------------------------- 
exit

set ORACLE_SID=TAINSTDB
set NLS_LANG=AMERICAN_AMERICA.AL32UTF8


echo "starting structure export ..." >>  log.log
date /t >> log.log
time /t >> log.log


exp userid=system full=y rows=n file=exp_tainstdb_structure_2012-11-15.dmp log=exp_tainstdb_structure_2012-11-15.log


echo "structure export finished." >> log.log
date /t >> log.log
time /t >> log.log


exp userid=system full=y rows=y consistent=y feedback=25000 file=exp_tainstdb_full_2012-11-15.dmp log=exp_tainstdb_full_2012-11-15.log


echo "full database export finished." >> log.log
date /t >> log.log
time /t >> log.log


pause