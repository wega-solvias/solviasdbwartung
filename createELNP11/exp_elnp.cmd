--------------------------------------------------  
-- Wega Informatik AG  
-- Purpose : ELNP11 Export Database
-- $Date$  
-- $Author$  
-- $HeadURL$  
-- $Revision$  
--------------------------------------------------
exit

set ORACLE_SID=ELNP
set NLS_LANG=AMERICAN_AMERICA.AL32UTF8


echo "starting structure export ..." >>  log.log
date /t >> log.log
time /t >> log.log


exp userid=system/solvias full=y rows=n file=exp_elnp_structure_2013-10-28.dmp log=exp_elnp_structure_2013-10-28.log


echo "structure export finished." >> log.log
date /t >> log.log
time /t >> log.log


exp userid=system/solvias full=y rows=y consistent=y feedback=25000 file=exp_elnp_full_2013-10-28.dmp log=exp_elnp_full_2013-10-28.log


echo "full database export finished." >> log.log
date /t >> log.log
time /t >> log.log


pause