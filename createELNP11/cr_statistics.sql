--------------------------------------------------  
-- Wega Informatik AG  
-- Purpose : Create Statistics for ELN
-- $Date$  
-- $Author$  
-- $HeadURL$  
-- $Revision$  
--------------------------------------------------

set serveroutput on size unlimited

declare
  cursor cur_process is select distinct owner from dba_segments where owner like 'CS%' and 1=1 order by owner;
  l_starttime date := sysdate;
  l_endtime   date;
  
  procedure outit(i_str varchar2) is
    l_str varchar2(255);	
  begin
    l_str := to_char(sysdate,'yyyy.mm.dd hh24:mi') ||' : '|| i_str;
    dbms_output.put_line(  l_str);
  end;
  
  
begin
  l_endtime := sysdate;
  outit('starting collecting statistics ...');
  for i in cur_process loop
    outit('collecting statistics for schema : '||i.owner);
	dbms_stats.gather_schema_stats(
		ownname=>i.owner,
		estimate_percent=>null,
		cascade=>true,
		method_opt=>'for all columns size 1'
	);
  end loop;
  outit('collecting statistics finished.');
end;
/

select last_analyzed,owner,table_name,num_rows from dba_tables  where owner like 'CS%' order by last_analyzed nulls last;