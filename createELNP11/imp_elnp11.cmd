

set ORACLE_SID=ELNP11
set NLS_LANG=AMERICAN_AMERICA.AL32UTF8


echo "starting full database from to import ..." >>  imp_ELNP11.log
date /t >> imp_ELNP11.log
time /t >> imp_ELNP11.log


rem sqlplus system/ElnSolvias13562013

rem imp userid=system/ElnSolvias13562013 parfile=imp_csccartridge.par commit=y ignore=y statistics=none file=exp_elnp_full_2013-10-28.dmp log=imp_elnp11_full_csccartridge_2013-10-28.log

rem imp userid=system/ElnSolvias13562013 parfile=imp_cs_notebook9.par indexes=n commit=y ignore=y statistics=none file=exp_elnp_full_2013-10-28.dmp log=imp_elnp11_full_csnotebook9_2013-10-28.log


rem imp userid=system/ElnSolvias13562013 parfile=imp_rest.par indexes=n commit=y ignore=y statistics=none file=exp_elnp_full_2013-10-28.dmp log=imp_elnp11_fromto_2013-10-28.log


echo "full database from to import finished." >> imp_ELNP11.log
date /t >> imp_ELNP11.log
time /t >> imp_ELNP11.log


pause