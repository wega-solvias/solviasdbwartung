rem wega informatik AG
rem $Name:  $
rem $Revision$
rem $Date$ $Author$
rem $Source: F:/cvs/cvsrepo/wegadba/solvias/rman/backup_hot_archivelog.cmd,v $

set LOGPATH=E:\BackupOracle
set CMDPATH=E:\BackupOracle\rman

set ORACLE_HOME=E:\oracle\ora92
set ORACLE_SID=SYMYX
set NLS_LANG=AMERICAN_AMERICA.WE8MSWIN1252

set CMDFILE=%CMDPATH%\backup_archivelog.rcv
set LOGFILE=%LOGPATH%\rman_archivelog.log

date /t >> %LOGFILE%
time /t >> %LOGFILE%

%ORACLE_HOME%\bin\rman TARGET / NOCATALOG CMDFILE %CMDFILE% >> %LOGFILE%

date /t >> %LOGFILE%
time /t >> %LOGFILE%




