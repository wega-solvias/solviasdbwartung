
# useful rman commands....
# wega Informatik AG
# $Name:  $
# $Revision$
# $Date$ $Author$
# $Source: F:/cvs/cvsrepo/wegadba/solvias/rman/usefulrmancommands.txt,v $

# Useful rman commands ...
list backup;
list backup of database;
list backup of tablespace SYSTEM; 
list backup of controlfile; 

list backup of archivelog all;

list backupset of database; 
list backupset of controlfile; 
list backupset of tablespace system;

report schema;

list copy of database archivelog all; 

report need backup days=2 database; 
report need backup days=10 tablespace TEMP; 
report schema;

show all;
show retention policy;

list backupset of datafile 13;

list backupset of datafile 13 completed after 'sysdate -4';


# To test the most recent RMAN database backup:
restore validate database;
