rem wega informatik AG
rem $Name:  $
rem $Revision$
rem $Date$ $Author$
rem $Source: F:/cvs/cvsrepo/wegadba/solvias/rman/backup_hot_archivelog.cmd,v $


set LOGPATH=E:\BackupOracle
set CMDPATH=E:\BackupOracle\rman

set ORACLE_HOME=E:\oracle\ora92
set ORACLE_SID=ENAHYD9
set NLS_LANG=AMERICAN_AMERICA.WE8ISO8859P1

set CMDFILE=%CMDPATH%\enahyd\backup_archivelog.rcv
set LOGFILE=%LOGPATH%\rman_archivelog_enahyd.log

date /t >> %LOGFILE%
time /t >> %LOGFILE%

%ORACLE_HOME%\bin\rman TARGET / NOCATALOG CMDFILE %CMDFILE% >> %LOGFILE%

date /t >> %LOGFILE%
time /t >> %LOGFILE%




