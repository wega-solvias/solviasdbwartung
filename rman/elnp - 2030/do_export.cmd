rem
rem Daily Full Export of ELNP Database
rem created 14.08.2008, hg wega Informatik AG
rem

set ORACLE_SID=ELNP
set NLS_LANG=AMERICAN_AMERICA.AL32UTF8
exp userid=system/xxxx file=(F:\BackupOracle\dailyexport\elnp_full1.dmp,F:\BackupOracle\dailyexport\elnp_full2.dmp,F:\BackupOracle\dailyexport\elnp_full3.dmp) log=F:\BackupOracle\elnp_full.log full=y consistent=y filesize=2G

time /t >> F:\BackupOracle\elnp_full.log
date /t >> F:\BackupOracle\elnp_full.log

