rem wega informatik AG
rem $Name:  $
rem $Revision$
rem $Date$ $Author$
rem $Source: /wegadba/solvias/rman/backup_hot_database_0.cmd,v $

set LOGPATH=F:\BackupOracle
set CMDPATH=F:\BackupOracle\scripts

set ORACLE_HOME=E:\oracle\product\10.2.0
set ORACLE_SID=ELNP
set NLS_LANG=AMERICAN_AMERICA.AL32UTF8


set CMDFILE=%CMDPATH%\backup_hot_db_level_0.rcv
set LOGFILE=%LOGPATH%\rman_elnp_level_0.log

date /t >> %LOGFILE%
time /t >> %LOGFILE%

%ORACLE_HOME%\bin\rman TARGET sys/xxxxx NOCATALOG CMDFILE %CMDFILE% >> %LOGFILE%

date /t >> %LOGFILE%
time /t >> %LOGFILE%