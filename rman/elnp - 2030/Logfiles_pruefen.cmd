@ECHO OFF
rem *Dieses cmd File überprüft die angegebenen Logfiles des Oracle Backups
rem *Die Position der letzten Überprüfung wird in checkpoints gespeichert
rem *Es werden die folgenden Logfiles überprüft
rem *
rem * Erstellt: Andreas Wilde, 07.03.2008	Version 1.0
rem *
rem *
rem *\\SO-S2036\Agilent$\DiverseLogFiles\rman_archivelog.log
rem *\\SO-S2036\Agilent$\DiverseLogFiles\rman_level_1_diff.log
rem *\\SO-S2036\Agilent$\DiverseLogFiles\rman_level_0.log

ECHO Ueberpruefung von rman_elnp_level_0.log
"C:\Program Files\Log Parser 2.2\logparser.exe" "SELECT text FROM F:\BackupOracle\rman_elnp_level_0.log WHERE Text LIKE '%%RMAN-%%' OR Text LIKE '%%ORA-%%'" -i:TEXTLINE -iCheckpoint:F:\BackupOracle\rman_elnp_level_0.lpc

ECHO Ueberpruefung von rman_elnp_level_1.log
"C:\Program Files\Log Parser 2.2\logparser.exe" "SELECT text FROM F:\BackupOracle\rman_elnp_level_1.log WHERE Text LIKE '%%RMAN-%%' OR Text LIKE '%%ORA-%%'" -i:TEXTLINE -iCheckpoint:F:\BackupOracle\rman_elnp_level_1.lpc

ECHO Ueberpruefung von rman_archivelog.log
"C:\Program Files\Log Parser 2.2\logparser.exe" "SELECT text FROM F:\BackupOracle\rman_archivelog.log WHERE Text LIKE '%%RMAN-%%' OR Text LIKE '%%ORA-%%'" -i:TEXTLINE -iCheckpoint:F:\BackupOracle\rman_archivelog.lpc

ECHO Ueberpruefung von elnp_full.log
"C:\Program Files\Log Parser 2.2\logparser.exe" "SELECT text FROM F:\BackupOracle\elnp_full.log WHERE Text LIKE '%%RMAN-%%' OR Text LIKE '%%ORA-%%' " -i:TEXTLINE -iCheckpoint:F:\BackupOracle\elnp_full.log.lpc

pause
