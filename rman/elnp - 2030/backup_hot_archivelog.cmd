rem wega informatik AG
rem $Name:  $
rem $Revision$
rem $Date$ $Author$
rem $Source: /wegadba/solvias/rman/elnp - 2030/backup_hot_archivelog.cmd,v $

set LOGPATH=F:\BackupOracle
set CMDPATH=F:\BackupOracle\scripts

set ORACLE_HOME=E:\oracle\product\10.2.0
set ORACLE_SID=ELNP
set NLS_LANG=AMERICAN_AMERICA.AL32UTF8

set CMDFILE=%CMDPATH%\backup_archivelog.rcv
set LOGFILE=%LOGPATH%\rman_archivelog.log

date /t >> %LOGFILE%
time /t >> %LOGFILE%

%ORACLE_HOME%\bin\rman TARGET sys/xxxxxx NOCATALOG CMDFILE %CMDFILE% >> %LOGFILE%

date /t >> %LOGFILE%
time /t >> %LOGFILE%




