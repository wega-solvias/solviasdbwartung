rem wega informatik AG
rem $Name:  $
rem $Revision$
rem $Date$ $Author$
rem $Source: /wegadba/solvias/rman/hpcs - 2033/backup_hot_system01.cmd,v $

set LOGPATH=F:\BackupOracle
set CMDPATH=F:\BackupOracle\rman

set ORACLE_HOME=E:\oracle\ora92
set ORACLE_SID=HPCS
set NLS_LANG=AMERICAN_AMERICA.WE8ISO8859P1

set CMDFILE=%CMDPATH%\backup_hot_system01_0.rcv
set LOGFILE=%LOGPATH%\rman_system01.log

date /t >> %LOGFILE%
time /t >> %LOGFILE%

%ORACLE_HOME%\bin\rman TARGET sys/xxxx NOCATALOG CMDFILE %CMDFILE% >> %LOGFILE%

date /t >> %LOGFILE%
time /t >> %LOGFILE%




