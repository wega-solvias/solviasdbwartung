
rem wega informatik AG
rem $Name:  $
rem $Revision$
rem $Date$ $Author$
rem $Source: F:/cvs/cvsrepo/wegadba/solvias/rman/backup_hot_database_0.cmd,v $

set LOGPATH=F:\BackupOracle
set CMDPATH=F:\BackupOracle\rman

set ORACLE_HOME=E:\oracle\ora92
set ORACLE_SID=HPCS
set NLS_LANG=AMERICAN_AMERICA.WE8ISO8859P1


set CMDFILE=%CMDPATH%\backup_hot_db_level_0.rcv
set LOGFILE=%LOGPATH%\hpcs_rman_level_0.log

date /t >> %LOGFILE%
time /t >> %LOGFILE%

%ORACLE_HOME%\bin\rman TARGET sys/xxx NOCATALOG CMDFILE %CMDFILE% >> %LOGFILE%

date /t >> %LOGFILE%
time /t >> %LOGFILE%