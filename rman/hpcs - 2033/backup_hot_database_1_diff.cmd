
rem wega informatik AG
rem $Name:  $
rem $Revision$
rem $Date$ $Author$
rem $Source: F:/cvs/cvsrepo/wegadba/solvias/rman/backup_hot_database_1_diff.cmd,v $

set LOGPATH=F:\BackupOracle
set CMDPATH=F:\BackupOracle\rman

set ORACLE_HOME=E:\oracle\ora92
set ORACLE_SID=HPCS
set NLS_LANG=AMERICAN_AMERICA.WE8ISO8859P1

set CMDFILE=%CMDPATH%\backup_hot_db_level_1_dif.rcv
set LOGFILE=%LOGPATH%\hpcs_rman_level_1_diff.log

date /t >> %LOGFILE%
time /t >> %LOGFILE%

%ORACLE_HOME%\bin\rman TARGET sys/hackbraten39 NOCATALOG CMDFILE %CMDFILE% >> %LOGFILE%

date /t >> %LOGFILE%
time /t >> %LOGFILE%



