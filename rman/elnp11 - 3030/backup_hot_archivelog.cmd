rem wega informatik AG
rem $Name:  $
rem $Revision$
rem $Date$ $Author$
rem $HeadURL$

set LOGPATH=F:\BackupOracle
set CMDPATH=F:\BackupOracle\scripts

set ORACLE_HOME=C:\oracle\product\11.2.0\dbhome_1
set ORACLE_SID=ELNP11
set NLS_LANG=AMERICAN_AMERICA.AL32UTF8

set CMDFILE=%CMDPATH%\backup_archivelog.rcv
set LOGFILE=%LOGPATH%\rman_archivelog.log

date /t >> %LOGFILE%
time /t >> %LOGFILE%

%ORACLE_HOME%\bin\rman TARGET sys/solvias NOCATALOG CMDFILE %CMDFILE% >> %LOGFILE%

date /t >> %LOGFILE%
time /t >> %LOGFILE%




