--------------------------------------------------  
-- Wega Informatik AG  
-- Project - Solvias - SQLServer Wartung
-- Purpose : Job Historie untersuchen
-- $Date$  
-- $Author$  
-- $HeadURL$  
-- $Revision$  
--------------------------------------------------
-- http://msdn.microsoft.com/de-de/library/ms174997.aspx
-- run_status 0-fehler, 1-erfolg, 2-wiederholen, 3-abgebrochen
-- sql_severity - Schweregrad des sqlfehlers
-- run_date - Datum an dem die Ausführung des Schrittes gestartet wurde
-- run_time - Uhrzeit an dem die Ausführung des Schrittes gestartet wurde
-- run_duration - Verstrichene Ausführungszeit in Format HHMMSS
-- server - name des servers

-- wega wartung
select * from msdb.dbo.sysjobhistory jh where job_id='1B4E1259-8F69-4FFC-8D3A-39A8D87E977C'

select * from msdb.dbo.sysjobs


select  
  jh.server,  
  j.name,  
  jh.step_id,  
  jh.step_name,  
  jh.run_status,  
  jh.run_date,  
  jh.run_time,  
  jh.run_duration,  
  j.enabled,  
  j.description,  
  jh.sql_severity,  
  jh.message  
 from msdb.dbo.sysjobhistory jh  
 inner join msdb.dbo.sysjobs j on jh.job_id = j.job_id   
order by run_date asc, run_time asc


select  
  j.name,  
  jh.step_id,  
  jh.step_name,  
  sum(jh.run_duration) duration
 from msdb.dbo.sysjobhistory jh  
 ,msdb.dbo.sysjobs j 
where jh.job_id = j.job_id   
and jh.run_date >= 20130120
and jh.run_date <  20130121
 group by j.name,jh.step_id,jh.step_name
order by  duration desc