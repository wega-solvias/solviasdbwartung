--------------------------------------------------  
-- Wega Informatik AG  
-- Purpose : Examine Eventlogs of SQLServer - Solvias Wartung
-- $Date$  
-- $Author$  
-- $HeadURL$  
-- $Revision$  
--------------------------------------------------

 
################################################################################################
### create check folder on W007
set-location C:\wega\monthlyCheck
mkdir "2016-04-13-sqlserver"

################################################################################################
### Start PowerShell on SQLServer (SO-S2008 or SO-S2077) and change directory
cd \\tsclient\c\wega\monthlyCheck\2016-04-13-sqlserver

get-psdrive -PSProvider Filesystem | Select-Object @{Name = "Drive" ; Expression={$_.Name}} `
, @{Name = "Used [GB]"; expression = { [Math]::Round($_.Used / 1gb ,2) }} `
, @{Name = "Free [GB]"; expression = { [Math]::Round($_.Free / 1gb ,2) }} `
, @{Name = "Used [%]"; expression = { [Math]::Round( $_.Used / ($_.Used + $_.Free) * 100 ,2 ) }}  `
| fl


################################################################################################
### define variables
$after     = get-date 1/10/16
$before    = get-date 4/13/16
$myfile  = "SolviasEventlog_SO-S2008.dat"

# Achtung anderes Datumsformat bei SO-S2077 !!!
$after     = get-date 28.10.15
$before    = get-date 27.11.15
$myfile  = "SolviasEventlog_SO-S2077.dat"


# Get-EventLog -LogName System -newest 1 | fl
$mySystemErrorEvents=Get-EventLog -LogName System  | where-Object {$_.TimeGenerated -gt $after} | where-Object {$_.TimeGenerated -lt $before} | where-Object {$_.EntryType -eq 'Error'} 
$myApplicationErrorEvents=Get-EventLog -LogName Application  | where-Object {$_.TimeGenerated -gt $after} | where-Object {$_.TimeGenerated -lt $before} | where-Object {$_.EntryType -eq 'Error'} 

#$SystemJob      = Start-Job -Name getSystemInfo -ScriptBlock {Get-EventLog -LogName System  | where-Object {$_.TimeGenerated -gt $after} | where-Object {$_.TimeGenerated -lt $before} | where-Object {$_.EntryType -eq 'Error'}  }
#$ApplicationJob = Start-Job -Name getAppInfo    -ScriptBlock {Get-EventLog -LogName Application  | where-Object {$_.TimeGenerated -gt $after} | where-Object {$_.TimeGenerated -lt $before} | where-Object {$_.EntryType -eq 'Error'}  }

#Wait-Job -job $SystemJob
#Wait-Job -job $ApplicationJob

#$mySystemErrorEvents = Receive-Job $SystemJob -keep
#$myApplicationErrorEvents = Receive-Job $ApplicationJob


################################################################################################
### Report on System Log

out-file $myfile -inputobject "##########################################################"
out-file $myfile -inputobject $hostname -append
out-file $myfile -inputobject "##########################################################"  -append
out-file $myfile -inputobject "## System Error Events" -append
$mySystemErrorEvents | group-object -property Source  | sort-object -property count -descending | out-file $myfile -append
$mySystemErrorEvents | group-object -property EventID | sort-object -property count -descending | out-file $myfile -append

################################################################################################
### Report on Application Log

out-file $myfile -inputobject "## Application Error Events" -append
$myApplicationErrorEvents | group-object -property Source  | sort-object -property count -descending| out-file $myfile -append
$myApplicationErrorEvents | group-object -property EventID | sort-object -property count -descending| out-file $myfile -append

################################################################################################
### Error Details

out-file $myfile -inputobject "##Details on Errors" -append

$fmt = @{Expression={$_.Message};Label="Message";width=70}, `
@{Expression={$_.EventID};Label="Event";width=20}, `
@{Expression={$_.TimeGenerated};Label="Time";width=20}, `
@{Expression={$_.Source};Label="Source";width=20}

################################################################################################
# SO-S2008
# #$mySystemErrorEvents      | where-Object {$_.Source -eq 'Eventlog'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
# $mySystemErrorEvents      | where-Object {$_.Source -eq 'volsnap'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
# $mySystemErrorEvents      | where-Object {$_.Source -eq 'PlugPlayManager'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append

$mySystemErrorEvents      | where-Object {$_.Source -eq 'iScsiPrt'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
$mySystemErrorEvents      | where-Object {$_.Source -eq 'UmrdpService'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
$mySystemErrorEvents      | where-Object {$_.Source -eq 'Service Control Manager'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
$mySystemErrorEvents      | where-Object {$_.Source -eq 'Schannel'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append

# $mySystemErrorEvents      | where-Object {$_.Source -eq 'TermDD'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
# $myApplicationErrorEvents | where-Object {$_.Source -eq 'Hiback'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
# $myApplicationErrorEvents | where-Object {$_.Source -eq 'SideBySide'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
# $myApplicationErrorEvents | where-Object {$_.Source -eq 'Application Error'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
# $myApplicationErrorEvents | where-Object {$_.Source -eq 'Application Hang'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append

# $myApplicationErrorEvents | where-Object {$_.Source -eq 'MSSQLServerOLAPService'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
# $myApplicationErrorEvents | where-Object {$_.Source -eq 'Navssprv'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
# $myApplicationErrorEvents | where-Object {$_.Source -eq 'SnapDrive'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
# $myApplicationErrorEvents | where-Object {$_.Source -eq 'Microsoft-Windows-CAPI2'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
# $myApplicationErrorEvents | where-Object {$_.Source -eq 'DatabaseMail'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append

$myApplicationErrorEvents | where-Object {$_.Source -eq 'MSSQLSERVER'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
$myApplicationErrorEvents | where-Object {$_.Source -eq 'SQLISPackage'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
$myApplicationErrorEvents | where-Object {$_.Source -eq 'EvntAgent'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
$myApplicationErrorEvents | where-Object {$_.Source -eq 'W3CTRS'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
$myApplicationErrorEvents | where-Object {$_.Source -eq 'Perflib'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append

# $myApplicationErrorEvents | where-Object {$_.Source -eq 'MSSQLServerOLAPService'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append

# $myApplicationErrorEvents | where-Object {$_.Source -eq 'Application Error'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append



################################################################################################
# SO-S2077

# $mySystemErrorEvents      | where-Object {$_.Source -eq 'Eventlog'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
# 
# $mySystemErrorEvents      | where-Object {$_.Source -eq 'NETLOGON'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
# $mySystemErrorEvents      | where-Object {$_.Source -eq 'DCOM'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
# $mySystemErrorEvents      | where-Object {$_.Source -eq 'Schannel'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
#$mySystemErrorEvents      | where-Object {$_.Source -eq 'iScsiPrt'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
#$mySystemErrorEvents      | where-Object {$_.Source -eq 'UmrdpService'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append

$mySystemErrorEvents      | where-Object {$_.Source -eq 'Service Control Manager'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append


# $myApplicationErrorEvents | where-Object {$_.Source -eq 'Hiback'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
# $myApplicationErrorEvents | where-Object {$_.Source -eq 'Microsoft-Windows-CAPI2'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
# $myApplicationErrorEvents | where-Object {$_.Source -eq 'SideBySide'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
# $myApplicationErrorEvents | where-Object {$_.Source -eq 'SO-S2077-CLASSIC'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
# $myApplicationErrorEvents | where-Object {$_.Source -eq 'SO-S2077-SQL'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
# $myApplicationErrorEvents | where-Object {$_.Source -eq 'DatabaseMail'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
# $myApplicationErrorEvents | where-Object {$_.Source -eq 'PerfNet'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append

$myApplicationErrorEvents | where-Object {$_.Source -eq 'Application Error'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
$myApplicationErrorEvents | where-Object {$_.Source -eq 'MSSQLSERVER'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
# $myApplicationErrorEvents | where-Object {$_.Source -eq 'SQLSIPackage'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append
# $myApplicationErrorEvents | where-Object {$_.Source -eq 'MSSQLServerOLAPService'} | sort-object -property EventID, TimeGenerated -descending | ft $fmt | out-string -width 4096 | out-file $myfile -append