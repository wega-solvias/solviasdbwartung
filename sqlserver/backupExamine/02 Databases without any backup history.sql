-------------------------------------------------------------------------------------------
--Databases without any backup history
-------------------------------------------------------------------------------------------
SELECT
       CONVERT(CHAR(100), SERVERPROPERTY('Servername')) AS Server,
       master.dbo.sysdatabases.NAME AS database_name,
       NULL AS [Last Data Backup Date],
       9999 AS [Backup Age (Hours)]
    FROM
       master.dbo.sysdatabases LEFT JOIN msdb.dbo.backupset
           ON master.dbo.sysdatabases.name  = msdb.dbo.backupset.database_name
    WHERE msdb.dbo.backupset.database_name IS NULL AND master.dbo.sysdatabases.name <> 'tempdb'
    ORDER BY
       msdb.dbo.backupset.database_name