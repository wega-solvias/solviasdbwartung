--------------------------------------------------  
-- Wega Informatik AG  
-- Purpose : HPCS Create User and Roles
-- $Date$  
-- $Author$  
-- $HeadURL$  
-- $Revision$  
-------------------------------------------------- 
 
 
CREATE ROLE "CS_ACCESS" IDENTIFIED BY VALUES 'DAC5CC0B26CADCB0';
 

CREATE USER "LIMSACCESS" IDENTIFIED BY VALUES '59F381EE747F964E'
TEMPORARY TABLESPACE "TEMP";

GRANT "CONNECT" TO "LIMSACCESS";


CREATE USER "CSINTERNAL" IDENTIFIED BY VALUES 'C401256D76A5512C'
DEFAULT TABLESPACE "DATA1"
TEMPORARY TABLESPACE "TEMP";


GRANT UNLIMITED TABLESPACE TO "CSINTERNAL";

GRANT "CONNECT" TO "CSINTERNAL";
GRANT "RESOURCE" TO "CSINTERNAL";
GRANT "DBA" TO "CSINTERNAL";
GRANT "CS_ACCESS" TO "CSINTERNAL" WITH ADMIN OPTION;


ALTER USER "CSINTERNAL" DEFAULT ROLE "DBA","CONNECT","CS_ACCESS","RESOURCE";
ALTER USER "LIMSACCESS" DEFAULT ROLE "CONNECT";


grant create view     to cs_access;
grant create session  to cs_access;
grant create synonym  to cs_access;
grant cs_access       to csinternal;



SQL> select * from dba_role_privs where grantee like 'LI%' order by grantee;

GRANTEE                        GRANTED_ROLE                   ADM DEF
------------------------------ ------------------------------ --- ---
LIMSACCESS                     CONNECT                        NO  YES

SQL> select * from dba_role_privs where grantee like 'CS%' order by grantee;

GRANTEE                        GRANTED_ROLE                   ADM DEF
------------------------------ ------------------------------ --- ---
CSINTERNAL                     DBA                            NO  YES
CSINTERNAL                     CONNECT                        NO  YES
CSINTERNAL                     RESOURCE                       NO  YES
CSINTERNAL                     CS_ACCESS                      YES YES

SQL> select * from dba_sys_privs where grantee like 'CS%' order by grantee;

GRANTEE                        PRIVILEGE                                ADM
------------------------------ ---------------------------------------- ---
CS_ACCESS                      CREATE VIEW                              NO
CS_ACCESS                      CREATE SESSION                           NO
CS_ACCESS                      CREATE SYNONYM                           NO
CSINTERNAL                     UNLIMITED TABLESPACE                     NO

SQL> select * from dba_sys_privs where grantee like 'LI%' order by grantee;

no rows selected


