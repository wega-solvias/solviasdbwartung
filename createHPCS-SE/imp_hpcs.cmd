--------------------------------------------------  
-- Wega Informatik AG  
-- Purpose : Import Database HPCS
-- $Date$  
-- $Author$  
-- $HeadURL$  
-- $Revision$  
--------------------------------------------------  
exit

set ORACLE_SID=HPCS
set NLS_LANG=AMERICAN_AMERICA.WE8ISO8859P1


echo "starting full database from to import ..." >>  imp_HPCS.log
date /t >> imp_HPCS.log
time /t >> imp_HPCS.log

imp userid=system file=exp_hpcs_full_2012-11-15.dmp log=imp_hpcs_full_2012-11-15.log fromuser=CSINTERNAL,LIMSACCESS touser=CSINTERNAL,LIMSACCESS


echo "full database from to import finished." >> imp_hpcs.log
date /t >> imp_hpcs.log
time /t >> imp_hpcs.log


pause