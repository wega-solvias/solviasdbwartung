--------------------------------------------------  
-- Wega Informatik AG  
-- Purpose : HPCS Change Init Parameters
-- $Date$  
-- $Author$  
-- $HeadURL$  
-- $Revision$  
-------------------------------------------------- 

-- HPCS Parameter Change

alter system set processes=300 scope=spfile;
alter system set sessions=335 scope=spfile;
alter system set sga_max_size=2001023000 scope=spfile;
alter system set log_checkpoint_interval=100000 scope=both;
alter system set db_file_multiblock_read_count=32 scope=both;
alter system set control_file_record_keep_time=30 scope=both;
alter system set transactions=368 scope=spfile;
alter system set max_rollback_segments=73 scope=spfile;
alter system set shared_servers=0 scope=both;
alter system set job_queue_processes=1 scope=both;


--alter system reset mts_servers scope=spfile sid='*';