--------------------------------------------------  
-- Wega Informatik AG  
-- Purpose : DBInfo ChemStore HPCS
-- $Date$  
-- $Author$  
-- $HeadURL$  
-- $Revision$  
-------------------------------------------------- 
select * from v$instance;
ALTER SESSION SET current_schema=CSINTERNAL;

/*  DBINFO.SQL   */
/* Created       */
/* Rev A.01.00   */ 
/* Modified  HCB A.01.10  03/18/04  */
/*  Information Script file used in the migration of a chemstore Database to B.03.01 */


SET ECHO off

set pagesize 5000
set linesize 180

col today new_value _date;
col dbname new_value _dbname;
select 	to_char(sysdate,'dd Mon yyyy hh24:mi:ss') today, name "DB Name" from v$database;

select DS_SCHVER "CS Version",
	DS_BUILD "Build Number", 
	DS_LASTID "Last Table ID Number"
from 	CS_DS;



/* See if there are any files in Recovery */
Prompt
Prompt NOTE ** Any Files in recovery mode will be listed here **
Prompt ** --------------------------------------------------- **
SELECT * FROM v$recover_file;

/* Log file Info */

SELECT member "Log File Members" FROM v$logfile;
SELECT name "Log file Names and Locations" FROM v$datafile;
SELECT value "Control Files" FROM v$parameter WHERE name = 'control_files';

select rng_id "ID"  , rng_nm "Study Name"
	from cs_rng
;

select us_id "User ID #", us_loginnm "User Logon Name"
	from cs_us
;

select * from 
(select count(rn_id) "Number of Runs"
	from cs_rn), 
(select count(rd_id) "Raw Data Files"
	from cs_rd),
(select count(inj_id) "Injections"
	from cs_inj),
(select count(cg_id) "Chromatograms"
	from cs_cg),
(select count(unique ic_host) "Clients"
	from cs_ic),
(select count(unique ic_nm) "Instruments"
	from cs_ic)
; 


select * from 
(select count(pk_id) "Peaks"
	from cs_pk),
(select count(cp_id) "Compounds"
	from cs_cp)
;


select * from 
(select count(*) "Audit Trail" 
	from cs_at),
(select max(rn_id) "Max rn_id Number"
	from cs_at)
;


select * from 
(select count(*) "Chromatograms"
	from cs_cg),
(select max(rn_id) "Max rn_id"
	from cs_cg),
(select max(cg_id) "Max cg_id"
	from cs_cg),
(select max(so_id) "Max so_id"
	from cs_cg)
;

select * from 
(select count(*) "Spectra"
	from cs_sp),
(select max(rd_id) "Max rd_id"
	from cs_sp),
(select max(sp_id) "Max sp_id"
	from cs_sp),
(select max(so_id) "Max so_id"
	from cs_sp)
;

select * from 
(select count(*) "Methods"
	from cs_mt),
(select max(mt_id) "Max cs_mt"
	from cs_mt)
;

select * from
(select count(*) "Sequences"
	from cs_sq),
(select max(sq_id) "Max sq_id"
	from cs_sq)
;

select * from 
(select count(*) "Stored Objects"
	from cs_so),
(select max(so_id) "Max so_id" 
	from cs_so)
;


select * from 
(select count(*) "Method Objects"
	from cs_mt_so),
(select max(mt_id) "Max mt_id"
	from cs_mt_so)
;

select * from 
(select count(*) "Raw Data Objects" 
	from cs_rd_so),
(select max(rd_id) "Max rd_id"
	from cs_rd_so)
;


select cfs_id "Custom Field ID", cfs_nm "Custom Field Name" 
	from cs_cfs
;

select Count(distinct fl_id) "Number of Filters" 
	from cs_fl
;

Select rt_id ID, rt_Nm "Report Name" 
	from cs_rt
;

Select count (distinct sm_nm) "Sample Names" 
	from cs_sm
;

select count(qy_id) "Stored Queries" 
	from cs_QY
;

select cs_rng.rng_nm "Study Name",count(cs_rn.rn_id) "# Run in"
	from cs_rng,cs_rn 
		where cs_rng.rng_id=cs_rn.rng_id 
			group by cs_rng.rng_nm
;

/* additional entries for archive details */

select * from 
(select count(distinct rd_id) "Archived Raw Data" 
	from cs_al),
(select count(distinct rn_id) "Archived Runs"
	from cs_al)
;

Prompt ** Are there any Achives Unit Setup to Run ?**
Prompt
Prompt ** Remember Wakeup is set @ 15 min on New Migration **
Prompt
Prompt ** Status of 5= Archive Pending on Wakeup or Timed **
Prompt ** Status of 10 = Dearchive Pending **

select AR_id "Arch Units", ar_ss "Status", ar_ssts "Last Changed"
 from cs_AR order by AR_id;



Select Count(rn_ID) " Number of Runs Archived"  
	from Cs_AL
;

select Count( RN_arss) " Never Archive" from cs_RN	
where Rn_arss = 15;

select Count( RN_arss) "Archive Failed" from cs_RN
where Rn_arss = 4;

select Count( RN_arss) " Archive Pending" from cs_RN
where Rn_arss = 5;

select Count( RN_arss) " Archived But not Deleted" from cs_RN
where Rn_arss = 6;

select Count( RN_arss) " Archive Running" from cs_RN
where Rn_arss = 7;

select Count( RN_arss) " Delete Failed" from cs_RN
where Rn_arss = 20;

select Count( RN_arss) " Delete Pending" from cs_RN
where Rn_arss = 21;
SET ECHO OFF


