#
#   ChemStore CS dbmk
#   Brian Zhou, Hewlett Packard Co., CASD
#
#   this script can run on both HP-UX and Win32 platform
#   either Perl4 or Perl5
#   that is why I did not user the nice "#!/usr/bin/perl"
#   since the position of perl binary may vary
#   the preferred way is using "perl $dbmk.pl ..."
#
#   Modified: Jijo George John 
#           1. Fix a bug on a german language NT machine
#           2. If the tablespace file size is > 3GB, breakdown the file to
#              3GB chunks. Added dbmkFileStr, dbmkRemoveFiles
#           3. Adding AUTOEXTEND for the table spaces. Usually 10% of size of 
#              the tablespace is auto extended.
#

# ---------------------------------------------------------------------------- +
# 25-Apr-01 MW Changes for Oracle 8
# ---------------------------------------------------------------------------- +

$dbmk = 'csDbmk';
#$sysHomeEnvName = 'CS_HOME';
#$sysHome2bin = 'SYS/';
$sysHome2dll = 'SYS';
$sysHome2exe = 'Chemstor/Install/';

# error cases handled by eval { ... die ... }
$err_create_schema = "create schema";
$err_add_user = "add user";
$err_create_rollback = "create rollback segments";
$err_create_tblspace = "create table spaces";
$err_load_catalog = "load Oracle catalog";
$err_create_database = "create database instance";
$err_create_service = "create database service";

require "csDbmkJavaPlsql.pl";

# ---------------------------------------------------------------------------- +
#   open $SVRMGRL and pipe command to it
#   result is logged into $LOG
#   return exit code of $SVRMGRL
#
sub svrmgr
{
    local($cmdfile) = @_;
    local($workfile) = "$TMPDIR/workfile.dba";
    local($retCode) = 0;

    #
    # if $cmdfile contains multiple lines, then they are commands
    # themselves and dbmkTrans always puts commands into $workfile
    #
    local($prefixCmds)	= "CONNECT / as sysdba;\n"
            . "SET ECHO ON;\n"
			. "WHENEVER SQLERROR EXIT;\n";
    local($postfixCmds)	= "EXIT;\n";
    &dbmkTrans($cmdfile, $workfile);

    # NT does not support open pipe and $? exit code,
    # use open() and search for error

    open(SVRMGRL, "$SVRMGRL /nolog < $workfile 2>&1 |");
    open(LOG, ">> $LOG");
    local($oldfh) = select(LOG); $| = 1; select($oldfh);
    while (<SVRMGRL>) {
	$retCode = 1 if m/ORA-\d\d\d\d\d:/;
	print LOG;
    }
    close SVRMGRL;
    close LOG;

    return $retCode;
}


# ---------------------------------------------------------------------------- +
#   open $SQLPLUS and pipe command to it
#   result is logged into $LOG
#   return exit code of $SQLPLUS
#
sub sqlplus
{
    local($cmdfile, $usrpasswd, $forgetErr) = @_;

    local($workfile, $writeToLog, $prefixCmds, $postfixCmds);
    #
    # Oracle .sql files don't need dbmkTrans;
    # if $cmdfile contains multiple lines, then they are commands
    # themselves and dbmkTrans always puts commands into $workfile
    #
    if ($cmdfile !~ m/\n/ && $cmdfile =~ m/^$ORACLE_HOME/) {
	$workfile = $cmdfile;
	$writeToLog = "> $null ";
    } else {
	$workfile =	"$TMPDIR/workfile.sql";
	$writeToLog =	">>$LOG ";
#	$prefixCmds .=	"CONNECT $usrpasswd;\n";
	$prefixCmds .=	"SET ECHO ON;\n";
	$prefixCmds .=	"WHENEVER SQLERROR EXIT 1;\n" unless $forgetErr;
	$postfixCmds =	"EXIT;\n";
	&dbmkTrans($cmdfile, $workfile);
    }

    # NT does not support open pipe and exit code, use system() instead
#    system("$SQLPLUS $usrpasswd < $workfile $writeToLog 2>&1");
    system("$SQLPLUS /nolog < $workfile $writeToLog 2>&1");

    return $?;
}


# ---------------------------------------------------------------------------- +
#   same function as Unix command dirname
#   implemented here to find where this script resides
#   so that ChemStore CS schema creation/drop executables
#   can be found
#
sub dirName
{
    $dirName = $_[0];
    $dirName =~ s:[^/\\]*$::;
    return $dirName if $dirName =~ m:^[/\\]$:;
    $dirName =~ s:[/\\]$::;
    $dirName = "." if $dirName eq "";
    return $dirName;
}


# ---------------------------------------------------------------------------- +
#   recursively make directory
sub mkdir_p
{
    local($drvName, $dirName) = @_;
    return unless $dirName =~ m:^[/\\]:;
    return if $dirName =~ m:^[/\\]$:;
    mkdir_p($drvName, dirName($dirName));
    mkdir($drvName . $dirName, 0777);
#    print "'$dirName'\n";
}


# ---------------------------------------------------------------------------- +
#   change backslash to slash, NT perl can understand slash
#
sub slash { $_[0] =~ s:\\:/:g; }


# ---------------------------------------------------------------------------- +
#   change slash to backslash
#
sub backslash { $_[0] =~ s:/:\\:g; }


# ---------------------------------------------------------------------------- +
#   environment ORACLE_HOME and SYS_HOME
#
sub dbmkEnv
{
    $ORACLE_HOME = $ENV{'ORACLE_HOME'};
    $ORACLE_HOME_NAME = "OraHome92";  # default
#    $SYS_HOME = $ENV{$sysHomeEnvName};
}


# ---------------------------------------------------------------------------- +
#   a couple of differences between Unix and Win32 are handled here
#   recognize Win32 by eval symlink
#
#   25-Apr-01 MW Change program names and path for oracle 8
#
sub dbmkPlatform
{
    eval { symlink("", ""); };
    if ($@)	# Win32, no support for symlink
    {
	$platform = "NT";
	$pathSep = "\\";
	&slash($SYS_HOME);
	$ENV{'PATH'} = "$SYS_HOME$pathSep$sysHome2dll;" . $ENV{'PATH'};
	&slash($ORACLE_HOME);
	$sv{'oracle_home'} = $ORACLE_HOME;
	&backslash($sv{'oracle_home'});

	# 25-Apr-01 MW
		# $SVRMGRL ="$ORACLE_HOME/bin/svrmgr23.exe";
		# $SQLPLUS="$ORACLE_HOME/bin/plus33.exe";
		# $admSqlDir = "RDBMS73/ADMIN";
	$SVRMGRL ="$ORACLE_HOME/bin/sqlplus.exe";
	$SQLPLUS="$ORACLE_HOME/bin/sqlplus.exe";
	$admSqlDir = "RDBMS/ADMIN";
        
        # 20-Apr-01 MW	
	#$pupbldDir = "DBS";
	$pupbldDir = "SQLPLUS/Admin";

	$dbsDir = "DATABASE";
	$null = "nul:";
	$pwd = "cmd /c cd";
	$TMPDIR = $ENV{'temp'}; &slash($TMPDIR);
	$dateCmd = 'cmd /c "echo. | date"';
	die "ORACLE_SID is limited to 4 chars on Windows NT,\n" .
	    "ORACLE_SID must begin with alphabetical character " .
	    "and followed by alphanumeric character(s)."
	    unless $ORACLE_SID =~ m/^[a-z]\w{0,3}$/i;
    	$SCHEMACREATE = "$SYS_HOME/${sysHome2exe}hpcrtb00.exe";
    	$SCHEMADROP = "$SYS_HOME/${sysHome2exe}hpdptb00.exe";
	unless (-r $SCHEMACREATE && -r $SCHEMADROP)
	{
	    print "Cannot find $SCHEMACREATE!\n";
	    print "Be sure -syshome option is correctly set and\n";
	    print "hpcrtb00.exe is under \$$SYS_HOME/$sysHome2exe!\n";
	    exit -1;
	}
    }
    else	# most likely Unix
    {

	$platform = "UX";
	eval
	{
	    if ((getpwuid($>))[0] ne "oracle")
	    {
		print "You must be oracle to run dbmk!\n";
		exit -1;
	    }
	};

	$pathSep = "/";
	$sv{'oracle_home'} = $ORACLE_HOME;
	$SVRMGRL ="$ORACLE_HOME/bin/sqlplus";
	$SQLPLUS="$ORACLE_HOME/bin/sqlplus";
	$admSqlDir = "rdbms/admin";
	$pupbldDir = "sqlplus/admin";
	$dbsDir = "dbs";
	$null = "/dev/null";
	$pwd = "pwd";
	$TMPDIR = "/tmp";
	$dateCmd = 'date';
	die "ORACLE_SID must begin with alphabetical character " .
	    "and followed by alphanumeric character(s)."
	    unless $ORACLE_SID =~ m/^[a-z]\w+$/i;
    	$SCHEMACREATE = "$SYS_HOME/${sysHome2exe}hpcrtb00";
    	$SCHEMADROP = "$SYS_HOME/${sysHome2exe}hpdptb00";
	unless (-r $SCHEMACREATE && -r $SCHEMADROP)
	{
	    print "Cannot find $SCHEMACREATE/$SCHEMADROP!\n";
	    print "Be sure -syshome option is correctly set and\n";
	    print "hpcrtb00/hpdptb00 is under \$$SYS_HOME/$sysHome2exe!\n";
	    exit -1;
	}

    }

    unless (-r $SVRMGRL && -r $SQLPLUS)
    {
	print "Please correctly set \$ORACLE_HOME or -dbhome option!\n";
	exit -1;
    }

    if (-w '.')
	{ $LOG = "./$dbmk.log"; }
    else
	{ $LOG = "$TMPDIR/$dbmk.log"; }
}


# ---------------------------------------------------------------------------- +
#   command line option processing
#
sub dbmkCmdline
{
    local($_);

    local($usage) = <<EOQ;
Usage: ${dbmk} [options] dbname oracle_sid
options:
-verbose			increase verbosity
-dbconfig ${dbmk}.cfg		database configuration file
-dbinitora init7.ora            database init.ora file
-dbhome dbhome			database home directory
-syshome syshome		system home directory
-skiplistener			skip adding ORACLE_SID to listener.ora
-intpwd intpwd			password for CONNECT sys
-revision                       print revision information.
EOQ

    while ($_ = shift)
    {
	if (m/^-verbose$/i)
	    { $verbose = 1; }
	elsif (m/^-skiplistener$/i)
	    { $skiplistener = 1; }
	elsif (m/^-dbconfig$/i)
	    { $sv{'dbmk_cfg'} = shift; }
	elsif (m/^-drop$/i)
	    { $drop = 1; }
	elsif (m/^-dbinitora$/i)
	    { $initora_src = shift; }
	elsif (m/^-dbhome$/i)
	    { $ORACLE_HOME = shift; }
	elsif (m/^-dbhomename$/i)
	    { $ORACLE_HOME_NAME = shift; }
	elsif (m/^-syshome$/i)
	    { $SYS_HOME = shift; }
	elsif (m/^-intpwd$/i)
	    { $intpwd = shift; }
	elsif (m/^-/)
	    { die "Invalid option $_\n$usage"; }
	elsif (!$sv{'dbname'})
	    { $sv{'dbname'} = $_; }
	else
	    { $sv{'oracle_sid'} = $_; }
    }

    die "No dbname!\n$usage"
	if !$sv{'dbname'};

    die "No oracle_sid!\n$usage"
	if !$sv{'oracle_sid'};
    $ORACLE_SID = $ENV{'ORACLE_SID'} = $sv{'oracle_sid'};

    die "No dbconfig!\n$usage"
	if !$sv{'dbmk_cfg'};

    if ($drop) {
	die "No intpwd!\n$usage"
	    unless $intpwd;
	return;
    }

    unless ($intpwd)
	 { $intpwd = 'oracle'; }

    die "Cannot read file $sv{'dbmk_cfg'}!\n"
	if !-r $sv{'dbmk_cfg'};

    $invokePath = &dirName($0);
    $initora_src = "$invokePath/init7.ora"
	unless $initora_src;

    die "Cannot read file $initora_src!\n"
	if !-r $initora_src;
}


# ---------------------------------------------------------------------------- +
#   log message to $LOG
#
sub logMsg
{
    open(LOG, ">> $LOG"); print LOG "====== @_"; close LOG;
}


# ---------------------------------------------------------------------------- +
#   print message to stdout if user like verbose output
#
sub logprtMsg
{
    logMsg @_;
    print @_ if $verbose;
}


# ---------------------------------------------------------------------------- +
#   parsing and storing config file
#
sub dbmkConfig
{
    local($configfile) = @_;

    # parms end with _path are supposed to be file path
    # these files will be removed if db creation not successful
    local(@parmList) = (
	"create_db_options",
	"control1_path", "control2_path",
	"sysdba", "sysdba_passwd",
	"logTbSp_a", "logTbSp_b", "logTbSp_c", "logTbSp_indx", "background_dump_path",
    "shared_pool_size", "java_pool_size", "core_dump_path", "user_dump_path"
    );

    open(CONFIG, "< $configfile");
    $toBeContinued = 0;
    while (<CONFIG>) {

	# strip comment, remove trailing blank
	s/\#.*$//;
	s/\s*$//;
	next if m/^\s*$/;

	unless ($toBeContinued)
	{
	    s/^\s*//;
	    # get parm name and value
	    ($parm, $v) = ($1, $2) if m/^(\S+)\s*(.*)$/;
	    if ($parm =~ m/^phyTbSp(\d+)(_name|_path|_size|_storage)$/) {
		$phyTbSp_max = ($1 > $phyTbSp_max) ? $1 : $phyTbSp_max;
	    } elsif (!grep($parm eq $_, @parmList)) {
		print "Unknown config parameter '$parm'!\n";
		exit -1;
	    }
	}
	else {
	    $v = $_;
	}

	# remove double quote
	$openQQ = ($v =~ s/^"//);
	$closeQQ = ($v =~ s/"$//);
	$toBeContinued = ( $toBeContinued || $openQQ ) && !$closeQQ;

	# interpolate ${var} in config value string
	while ($v =~ m/\$\{(\w+)\}/) {
	    $sv = $1;
	    unless (grep($sv eq $_, keys(%sv))) {
		print "Unknown substitute parameter \$\{$sv\} in $configfile!";
		exit -1;
	    }
	    $v =~ s/\$\{$sv\}/$sv{$sv}/e;
	}

	# check _storage syntax

	# put it into hash
	$parm{$parm} .= "\n" if $parm{$parm};
	$parm{$parm} .= $v;

    }
    close CONFIG;

    # check for any missing parm
    for ($i=1; $i<=$phyTbSp_max; $i++) {
	push(@parmList,
	    "phyTbSp${i}_name",
	    "phyTbSp${i}_path",
	    "phyTbSp${i}_size",
	    "phyTbSp${i}_storage");
    }

    foreach (@parmList) {
	if (!defined($parm{$_})) {
	    print "Missing config parameter '$_'!\n";
	    exit -1;
	}
	if (m/^phyTbSp_/) {
	    $v = $parm{$_};
	    unless ($v =~ m/^phyTbSp(\d+)/) {
		print "value of $_ '$v' not found!\n";
		exit -1;
	    }
	    $i = $1;
	    unless (1 <= $i && $i <= $phyTbSp_max) {
		print "value of $_ '$v' not in range!\n";
		exit -1;
	    }
	    $parm{$_} = $parm{"${v}_name"};
	}
	else {
	    $parm{$_} = 1 if $parm{$_} eq "ON";
	    $parm{$_} = 0 if $parm{$_} eq "OFF";
	}
    }

    %parm = ( %parm, %sv );

    $initora_dest = "${ORACLE_HOME}/$dbsDir/init${ORACLE_SID}.ora";
    $shared_pool_size = $parm{'shared_pool_size'};
    $java_pool_size = $parm{'java_pool_size'};


    # prepare ${rollback_segments} for recreating init${oracle_sid}.ora
#    $parm{'rollback_segments'} = '';
#    for $i (1 .. $parm{'rollback_count'}) {
#	$parm{'rollback_segments'} .= ', ' if $parm{'rollback_segments'};
#	$parm{'rollback_segments'} .= "RS$i";
#    }

    $_ = $parm{'create_db_options'};
    $fileCnt = 0; while (s/^[^']*'([^']*)'//) { $fileCnt++; $parm{"${fileCnt}_path"} = $1; }

# foreach (sort keys(%parm)) { printf "%-30s%s$parm{$_}\n", $_, $parm{$_} =~ /\n/?"\n":"\t"; }; exit 0;

}


# ---------------------------------------------------------------------------- +
#   translate(interpolate) ${var} into value of $parm{'var'}
#   there are two types of translation
#	1. var oriented,
#	    for instance ${dbname} becomes DB after translation.
#	2. line oriented
#	    $%{rollback_enabled} rollback_segments = ( ${rollback_segments} )
#	   the output has
#	    rollback_segments = ( ${rollback_segments} )
#	   if $parm{rollback_enabled};
#	   the output does not contain this line if !$parm{rollback_enabled}
#
sub dbmkTrans
{
    local($src, $dest, $keepComment) = @_;

    open(DEST, "> $dest");

    print DEST $prefixCmds if $prefixCmds;

    if ($src =~ m/\n/) {

	local($tmpSrc) = "$TMPDIR/tmpCmdFile";
	open(SRC, "> $tmpSrc");
	print SRC $src;
	close SRC;
	$src = $tmpSrc;

    }

    open(SRC, "< $src");
    while (<SRC>) {
        unless ($keepComment) {	# comment and blank line
	    s/#.*$//; s/^\s*(.*)/$1/; next if m/^$/;
        }
        next if (s/^\$%\{(\w+)\}\s*// && !$parm{$1});
        while (s/\$\{(\w+)\}/$parm{$1}/) {};
        print DEST;
    }
    close SRC;

    print DEST $postfixCmds if $postfixCmds;

    close DEST;
}


# ---------------------------------------------------------------------------- +
#   restart the database instance
#
sub restart
{
    &logprtMsg("Restarting the database instance.\n");
    &svrmgr(<<"EDQ");
	SHUTDOWN;
	STARTUP PFILE = \${oracle_home}/$dbsDir/init\${oracle_sid}.ora;
EDQ
}


# ---------------------------------------------------------------------------- +
sub OracleService
{
    local($pfile) = @_;
    local($syscmd);
    
    return (-1) unless -f $pfile;
    
    $pfile =~ s:/:$pathSep:g;

    $serviceStatus = 0;
    
    $service = "OracleService$ORACLE_SID";

    system("NET START $service 2>>$LOG ");
    return( -3 ) if $? = 0;
    &logprtMsg("$_") if $? != 0;

# 25-Apr01 MW 
    #system("$ORACLE_HOME/bin/ORADIM73 "
    #    . "-NEW "
    #    . "-SID $ORACLE_SID "
    #    . "-INTPWD $intpwd "
    #    . "-STARTMODE AUTO "
    #    . "-PFILE $pfile");

    $syscmd = "$ORACLE_HOME/bin/ORADIM "
        . "-NEW "
        . "-SID $ORACLE_SID "
        . "-INTPWD $intpwd "
        . "-STARTMODE MANUAL";
    #system("$ORACLE_HOME/bin/ORADIM "
    #    . "-NEW "
    #    . "-SID $ORACLE_SID "
    #    . "-INTPWD $intpwd "
    #    . "-STARTMODE MANUAL "
    #    . "-PFILE $pfile");

    # &logprtMsg("\nExecuting: " . $syscmd . "\n");
    system($syscmd);
    &logprtMsg("Error on creating service $service") if $? != 0;
    
    return($serviceStatus);
}


# ---------------------------------------------------------------------------- +
#   create the database instance
#   load database catalog
#   make available Oracle standard packages
#
sub dbmkPhysical
{
    &logprtMsg(
	"Creating the \$ORACLE_HOME/$dbsDir/init${ORACLE_SID}.ora file\n");
    &dbmkTrans($initora_src, $initora_dest, 'keepComment');

    if ($platform eq "NT") {
	&logprtMsg("Creating instance service.\n");
	$serviceStatus = &OracleService($initora_dest);
	if ($serviceStatus < 0) {
	    die "$err_create_service, stopped";
	}
    }

    foreach (grep(m/_path$/ && ($_=$parm{$_}), keys %parm))
    {
	&slash($_); m/^([a-z]:)(.*)/i; &mkdir_p($1, $2);
    }

    # MW Add Shutdown abort
    &logprtMsg("Creating the specified database.\n");
    if (&svrmgr(<<"EDQ")) { die "$err_create_database, stopped"; }
    SHUTDOWN ABORT;
	STARTUP FORCE NOMOUNT PFILE = \${oracle_home}/$dbsDir/init\${oracle_sid}.ora;
	CREATE DATABASE \${dbname} \${create_db_options};
EDQ

    &logprtMsg("Testing the connection to database $ORACLE_SID.\n");

    &logprtMsg("Loading the database catalog.\n");

    # catalog.sql calls other .sql script
    # have to change directory to $ORACLE_HOME/$admSqlDir before calling it
    local($dir) = `$pwd`; chop($dir);

    &logprtMsg("Processing \$ORACLE_HOME/$admSqlDir/catalog.sql\n");
    chdir("$ORACLE_HOME/$admSqlDir");
    if (&sqlplus("CONNECT $SYS as sysdba;\n\@$ORACLE_HOME/$admSqlDir/catalog.sql\n", $SYS, 'forgetErr'))
	{ chdir($dir); die "$err_load_catalog, stopped"; }
    chdir($dir);

    &logprtMsg("Processing \$ORACLE_HOME/$admSqlDir/catproc.sql\n");
    chdir("$ORACLE_HOME/$admSqlDir");
    if (&sqlplus("CONNECT $SYS as sysdba;\n\@$ORACLE_HOME/$admSqlDir/catproc.sql\n", $SYS, 'forgetErr'))
	{ chdir($dir); die "$err_load_catalog, stopped"; }
    chdir($dir);

    &logprtMsg("Processing \$ORACLE_HOME/$admSqlDir/catdbsyn.sql\n");
    chdir("$ORACLE_HOME/$admSqlDir");
    if (&sqlplus("CONNECT $SYS as sysdba;\n\@$ORACLE_HOME/$admSqlDir/catdbsyn.sql\n", $SYS, 'forgetErr'))
	{ chdir($dir); die "$err_load_catalog, stopped"; }
    chdir($dir);

    &logprtMsg("Processing \$ORACLE_HOME/$pupbldDir/pupbld.sql\n");
    if (&sqlplus("CONNECT $SYSTEM;\n\@$ORACLE_HOME/$pupbldDir/pupbld.sql\n", $SYSTEM, 'forgetErr'))
	{ die "$err_load_catalog, stopped"; }

    &restart();
	
    system("$ORACLE_HOME/bin/ORADIM "
        . "-EDIT "
        . "-SID $ORACLE_SID "
        . "-STARTMODE AUTO ");

    $service = "OracleService$ORACLE_SID";
    system("NET START $service 2>>$LOG ");
    &logprtMsg("$_") if $? != 0;

    return 0;
}

# ---------------------------------------------------------------------------- +
#   create the database spfile
sub dbmkSpfile()
{

    &logprtMsg("Creating the SPFILE $ORACLE_HOME/$dbsDir/spfile${ORACLE_SID}.ora file\n");
    
    if (&svrmgr(<<"EDQ")) { die "creating spfile failed, stopped"; }
    SHUTDOWN;
    CREATE SPFILE = '\${oracle_home}/$dbsDir/spfile\${oracle_sid}.ora' FROM PFILE = '\${oracle_home}/$dbsDir/init\${oracle_sid}.ora';
    STARTUP;
    ALTER SYSTEM SET SHARED_POOL_SIZE=\${shared_pool_size} SCOPE=SPFILE;
    ALTER SYSTEM SET JAVA_POOL_SIZE=\${java_pool_size} SCOPE=SPFILE;
    SHUTDOWN;
    STARTUP;
EDQ
    &logprtMsg("renaming '$ORACLE_HOME/$dbsDir/init$ORACLE_SID.ora' to '$ORACLE_HOME/$dbsDir/init$ORACLE_SID._ora'\n");
    rename("$ORACLE_HOME/$dbsDir/init$ORACLE_SID.ora","$ORACLE_HOME/$dbsDir/init$ORACLE_SID._ora");

    return 0;
}



# ---------------------------------------------------------------------------- +
#   dbmkFileStr
#   This function generates the file information for the tablespace
#   about to be created. If the file size is > 2000M, it breaks it down
#   to smaller files.
#
sub dbmkFileStr
{
    local( $path_str, $size_str ) = @_;

    # The default string and maximum size are initialized here
    $data_str = " '$path_str' size $size_str ";
    $max_size = 2000;

    # If the size string has a M ( Mega bytes ), do the processing.
    # Otherwise, it is treated as an unknown format and will work
    # the old way
    if( $size_str =~ /M/ )
    {
        # Checking for the proper size
        $total_size = $`;
        $ten_perc = $size_str / 10 . "M";
        $data_str = $data_str . " AUTOEXTEND ON NEXT $ten_perc ";
        if( $total_size > $max_size )
        {
            # Initializing all the variables
            $data_str = " ";
            $remain_size = $total_size;
            $index = 1;
            $comma_str = " ";

            # Loop thru till we finish thru the size we were looking for
            while( $remain_size >= $max_size )
            {
                $data_str = $data_str . $comma_str . "'" . $path_str .
			"_" . $index . ".dbf'" . " size " . $max_size . "M";
                $remain_size = $remain_size - $max_size;
                $index++;
                $comma_str = ", ";
            }

            # The last chunk will have the leftover
            if( $remain_size > 0 )
            {
                $remain_size = $remain_size;
                $data_str = $data_str . $comma_str . "'" . $path_str .
		    "_" . $index . "'" . " size " . $remain_size . "M";
            }
            $data_str = $data_str . " AUTOEXTEND ON NEXT 200M ";
        }
    }

    return $data_str;
}


# ---------------------------------------------------------------------------- +
#   creating tablespaces
#   adding tablespace for rollback segments
#   adding rollback segments
#
sub dbmkStructural
{
    local($sql);

    &logprtMsg("Adding tablespaces and rollback segments.\n");

    &logprtMsg("Adding tablespaces.\n");

    $sql = "";
    for ($i=1; $i<=$phyTbSp_max; $i++) {
	$fileStr =  &dbmkFileStr( $parm{"phyTbSp".$i."_path"},
			$parm{"phyTbSp".$i."_size"} );
	$sql .= <<"EDQ";
    CONNECT $SYS as sysdba
	CREATE TABLESPACE \${phyTbSp${i}_name}
	DATAFILE $fileStr
	\${phyTbSp${i}_storage};
EDQ
    }
    if (&sqlplus($sql, $SYSTEM))
	{ die "$err_create_tblspace, stopped"; }

if (0)
{
    &logprtMsg("Adding rollback segments.\n");

    $rollback = "rollback";
    $sql = <<"EDQ";
	CREATE TABLESPACE $rollback
	DATAFILE '\${rollback_path}' SIZE \${rollback_size} REUSE;
EDQ
    foreach $i (1..$parm{'rollback_count'}) {
        $sql .= <<"EDQ";
	CREATE PUBLIC ROLLBACK SEGMENT RS$i
	TABLESPACE $rollback STORAGE (\${rollback_storage});
EDQ
    }
    if (&sqlplus($sql, $SYSTEM))
	{ die "$err_create_rollback, stopped"; }
}


    &logprtMsg(
	"Recreating the \$ORACLE_HOME/$dbsDir/init${ORACLE_SID}.ora file\n");
    # $parm{'rollback_enabled'} = 0;
    &dbmkTrans($initora_src, $initora_dest, 'keepComment');
    &restart();
}


# ---------------------------------------------------------------------------- +
#   create user ${sysdba}
#   invoke $SCHEMACREATE to create ChemStore CS schema
#
sub dbmkLogical
{
    &logprtMsg("Adding users.\n");
    if (&sqlplus(<<"EDQ", $SYSTEM)) { print "$err_add_user, stopped"; }
    CONNECT $SYSTEM;
	CREATE USER \${sysdba} IDENTIFIED BY \${sysdba_passwd} DEFAULT TABLESPACE \${phyTbSp1_name};
	GRANT CONNECT, RESOURCE, DBA TO \${sysdba};
EDQ

    &logprtMsg("Creating ChemStore CS database schema.\n");

    chop($hostname = `hostname`);
#     print '${hostname}_${ORACLE_SID} ' . "${hostname}_${ORACLE_SID}\n";

    system( "$SCHEMACREATE " .
	    "$parm{sysdba} " .
	    "$parm{sysdba_passwd} " .
	    "${hostname}_${ORACLE_SID} " .
	    "$parm{logTbSp_a} " .
	    "$parm{logTbSp_b} " .
	    "$parm{logTbSp_c} " .
	    "$parm{logTbSp_indx} " .
	    "DBG_DBORA " .
	    ">> $LOG 2>&1");

    if ($?) { die "$err_create_schema, stopped"; }

    &restart();
}


# ---------------------------------------------------------------------------- +
#   dbmkRemoveFiles
#   Removes the table space files for a drop command, if the table space
#   size specified was > 2000M
#
sub dbmkRemoveFiles
{
    local( $path_str, $size_str ) = @_;

    # The maximum size is initialized here
    $max_size = 2000;

    # If the size string has a M ( Mega bytes ), do the processing.
    # Otherwise, it is treated as an unknown format and will work
    # the old way
    if( $size_str =~ /M/ )
    {
        # Checking for the proper size
        $total_size = $`;
        if( $total_size > $max_size )
        {
            # Initializing all the variables
            $remain_size = $total_size;
            $index = 1;

            # Remove the files with the max_size
            while( $remain_size >= $max_size )
            {
		unlink $path_str."_" . $index;
                $remain_size = $remain_size - $max_size;
                $index++;
            }

            # Remove the last chunk
            if( $remain_size > 0 )
            {
		unlink $path_str."_" . $index;
            }
        }
    }

    return $data_str;
}


# ---------------------------------------------------------------------------- +
sub dpDatabase
{
    &logprtMsg("Removing database.\n");
    if (&svrmgr(<<"EDQ")) { die "error dropping database, stopped"; }
	SHUTDOWN ABORT;
EDQ
    # 25-Apr-01 MW
    #system("$ORACLE_HOME/bin/ORADIM73 -DELETE -SID $ORACLE_SID");
    system("$ORACLE_HOME/bin/ORADIM -DELETE -SID $ORACLE_SID");
    foreach (grep(m/_path$/ && ($_=$parm{$_}), keys %parm))
    {
	&slash($_); unlink $_ if -w $_;
    }

    # The data may be spread across different files. Added this code,
    # to complete the removal.
    for ($i=1; $i<=$phyTbSp_max; $i++) {
	&dbmkRemoveFiles( $parm{"phyTbSp".$i."_path"},
			$parm{"phyTbSp".$i."_size"} );
    }
}


# ---------------------------------------------------------------------------- +
sub addListener
{
    local($ORACLE_HOME, $newSID) = @_;

    local($addressList, @addressList) = (0);
    local($sidList, @sidList) = (0);
    local($indent, $read) = ('', '');

    local($listener_ora) = "$ORACLE_HOME/network/admin/listener.ora";
    local($listener_bak) = "$ORACLE_HOME/network/admin/listener.bak";
    local($listener_new) = "$ORACLE_HOME/network/admin/listener" . time;

    open LISTENER_ORA, "< $listener_ora";

    while (<LISTENER_ORA>)
    {
	next if m/^\s*#/;

	if (m/\(\s*ADDRESS_LIST/i)
    	    { $addressList = 1; }

	elsif (m/\s*SID_LIST/i)
    	    { $addressList = 0; $sidList = 1; }

	elsif (m/^(\s*)\(\s*(ADDRESS|SID_DESC)\s*=/)
	    { $indent = length($1); }

	if ($addressList && m/KEY\s*=\s*([\w.]+)\s*\)/)
	    { push @addressList, $1; }

	if ($sidList && m/SID_NAME\s*=\s*([\w.]+)\s*\)/)
	    { push @sidList, $1; }

    }

    close LISTENER_ORA;

    local($missingAddress) = !grep(m/^$newSID$/, @addressList);
    local($missingSID) = !grep(m/^$newSID$/, @sidList);
    return 1 if !$missingAddress && !$missingSID;

    open LISTENER_ORA, "< $listener_ora";
    open NEW_LISTENER, "> $listener_new";

    while (<LISTENER_ORA>)
    {
	print NEW_LISTENER $_;

	next if m/^\s*#/;

	if (m/^(\s*)\(\s*(ADDRESS|SID_DESC)\s*=/)
	    { $indent = $1; }

	if ($missingAddress && m/KEY\s*=\s*$addressList[$#addressList]/i)
	{
	    undef $read;
	    $brackets = 0;
	    $_ = $';
	    do {
		print NEW_LISTENER if $read;
		while (s/^[^()]*(\(|\))//) {
		    if ($1 eq '(') { $brackets--; } else { $brackets++; }
		}
	    } while ($brackets != 2 && defined($read=1, $_=<LISTENER_ORA>));
	    if (!defined($_)) {
		unlink($listener_new);
		return -2;
	    }
	    print NEW_LISTENER $indent, "(ADDRESS =\n";
	    print NEW_LISTENER $indent, "  (PROTOCOL= IPC)\n";
	    print NEW_LISTENER $indent, "  (KEY= $newSID)\n";
	    print NEW_LISTENER $indent, ")\n";
	    next;
	}

	if ($missingSID && m/SID_NAME\s*=\s*$sidList[$#sidList]/i)
	{
	    undef $read;
	    $brackets = 0;
	    $_ = $';
	    do {
		print NEW_LISTENER if $read;
		while (s/^[^()]*(\(|\))//) {
		    if ($1 eq '(') { $brackets--; } else { $brackets++; }
		}
	    } while ($brackets != 2 && defined($read=1, $_=<LISTENER_ORA>));
	    if (!defined($_)) {
		unlink($listener_new);
		return -2;
	    }
	    print NEW_LISTENER $indent, "(SID_DESC =\n";
	    print NEW_LISTENER $indent, "  (SID_NAME = $newSID)\n";
	    print NEW_LISTENER $indent, ")\n";
	    next;
	}

    }

    close LISTENER_ORA;
    close NEW_LISTENER;

    unlink($listener_bak);
    return -3 if !rename($listener_ora, $listener_bak);
    return -3 if !rename($listener_new, $listener_ora);

    local($service) = "Oracle" . "$ORACLE_HOME_NAME" . "TNSListener";
    system("NET STOP  $service 2>&1");
    system("NET START $service 2>&1");
    
    return 0;
}


# ---------------------------------------------------------------------------- +
sub dbmkMain
{
    local(%sv, %parm);

    &logMsg("\n");


    # Read Arguments
    @ARGV_SAV = @ARGV;


    # get Oracle Home directory
    &dbmkEnv;


    # Read command line arguments
    &dbmkCmdline(@ARGV);


    # Set plattform dependence variables
    &dbmkPlatform;


    # Write LogBook
    $now = localtime; chop($now = `$dateCmd`) unless $now;
    &logMsg("======= $now ================================\n");
    &logMsg(join(" ", $0, @ARGV_SAV) . "\n");


&logprtMsg("parsing and storing config file!\n");
    # parsing and storing config file
    &dbmkConfig($sv{'dbmk_cfg'});


    # Remove database
    if ($drop) {
	&dpDatabase();
	return;
    }


&logprtMsg("Modify Listener!\n");

    # Modify Listener
    if (!$skiplistener) {
	if (addListener($ORACLE_HOME, $ORACLE_SID) < 0) {
	    &logprtMsg("Having problem adding ORACLE_SID $ORACLE_SID " .
		"to $ORACLE_HOME/network/admin/listener.ora.\n");
	}
    }



    undef %sv;
    local($SYS) = "SYS/CHANGE_ON_INSTALL";
    local($SYSTEM) = "SYSTEM/MANAGER";
    local($SYSDBA) = "$parm{sysdba}/$parm{sysdba_passwd}";


    eval
    {
    &logprtMsg("IMPORTANT INFORMATION:\n");
    &logprtMsg("The database creation and configuration might take up to 1 to 2 hours,\n");
    &logprtMsg("including several steps where no indication of progress is visible on the screen\n\n");
    
	&logprtMsg("Start create physical DB!\n");
	&dbmkPhysical;

	&logprtMsg("Start create structural DB!\n");
	&dbmkStructural;

	&logprtMsg("Start create logical DB!\n");
	&dbmkLogical;

	&logprtMsg("Start loading java/plsql!\n");
	&dbmkJavaPlsql();

    # create spfile now
    &dbmkSpfile();

	$now = localtime; chop($now = `$dateCmd`) unless $now;
	&logMsg("======= $now ================================\n");
	&logprtMsg("ChemStore CS database has been successfully created!\n");
    };

    if ($@)
    {
	&logprtMsg("Error occurred. Please see $LOG for detail.\n");

	if (!$@ || $@ =~ m/^$err_create_schema/) {
	    &logprtMsg("Dropping database schema.\n");
	    system("$SCHEMADROP " .
		    "$parm{sysdba} " .
		    "$parm{sysdba_passwd} " .
		    "$ORACLE_SID " .
		    "DBG_DBORA " .
		    ">>$LOG 2>&1");
	    undef $@;
	}

	if (!$@ || $@ =~ m/^$err_add_user/) {
	    &logprtMsg("Dropping user.\n");
	    &sqlplus(
		"CONNECT $SYSTEM;\nDROP USER \${sysdba} CASCADE;\n",
		$SYSTEM,
		'forgetErr');
	    undef $@;
	}

	if (!$@ || $@ =~ m/^$err_create_rollback/) {
#	    &logprtMsg("Dropping rollback segments.\n");
#	    &sqlplus(
#		"DROP TABLESPACE $rollback INCLUDING CONTENTS;\n",
#		$SYSTEM,
#		'forgetErr');
	    undef $@;
	}

	if (!$@ || $@ =~ m/^$err_create_tblspace/) {
	    &logprtMsg("Dropping tablespaces.\n");
	    $sql = "";
	    for ($i=1; $i<=$phyTbSp_max; $i++) {
		$sql .= <<"EDQ";
	DROP TABLESPACE \${phyTbSp${i}_name} INCLUDING CONTENTS;
EDQ
	    }
	    &sqlplus($sql, $SYSTEM, 'forgetErr');
	    undef $@;
	}

	if (!$@ || $@ =~ m/^$err_load_catalog/) {
	    undef $@;
	}

	if (!$@ || $@ =~ m/^$err_create_database/) {
	    &dpDatabase();
	    undef $@;
	}

	if (!$@ || $@ =~ m/^$err_create_service/) {
	    undef $@;
	}
    }

}

&dbmkMain();

exit;

