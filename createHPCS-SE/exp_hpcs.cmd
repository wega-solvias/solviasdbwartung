--------------------------------------------------  
-- Wega Informatik AG  
-- Purpose : Export Database HPCS
-- $Date$  
-- $Author$  
-- $HeadURL$  
-- $Revision$  
-------------------------------------------------- 
exit

set ORACLE_SID=HPCS
set NLS_LANG=AMERICAN_AMERICA.WE8ISO8859P1


echo "starting structure export ..." >>  log.log
date /t >> log.log
time /t >> log.log


exp userid=system full=y rows=n file=exp_hpcs_structure_2012-11-15.dmp log=exp_hpcs_structure_2012-11-15.log


echo "structure export finished." >> log.log
date /t >> log.log
time /t >> log.log


exp userid=system full=y rows=y consistent=y feedback=25000 file=exp_hpcs_full_2012-11-15.dmp log=exp_hpcs_full_2012-11-15.log


echo "full database export finished." >> log.log
date /t >> log.log
time /t >> log.log


pause