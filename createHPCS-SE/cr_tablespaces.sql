--------------------------------------------------  
-- Wega Informatik AG  
-- Purpose : HPCS Create Tablespaces
-- $Date$  
-- $Author$  
-- $HeadURL$  
-- $Revision$  
-------------------------------------------------- 

-- UNDOTBS - 2048 M
-- INDX    - 1750 M
-- SYSTEM  -  250 M


alter database datafile 'G:\ORACLE\HPCS\HPCS_UNDOTBS01.DBF' resize 2048M;
alter database datafile 'G:\ORACLE\HPCS\HPCS_INDX01.DBF'   resize 2048M;


create tablespace DATA1 datafile 'g:\oracle\hpcs\HPCS_DATA1_01.DBF' size 4096M AUTOEXTEND OFF LOGGING ONLINE PERMANENT EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;
create tablespace DATA3 datafile 'g:\oracle\hpcs\HPCS_DATA3_01.DBF' size 1024M AUTOEXTEND OFF LOGGING ONLINE PERMANENT EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;

create tablespace DATA2     datafile 'g:\oracle\hpcs\HPCS_DATA2_01.DBF' size 2048M AUTOEXTEND OFF LOGGING ONLINE PERMANENT EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;
alter  tablespace DATA2 add datafile 'g:\oracle\hpcs\HPCS_DATA2_02.DBF' size 2048M autoextend off;
alter  tablespace DATA2 add datafile 'g:\oracle\hpcs\HPCS_DATA2_03.DBF' size 2048M autoextend off;
alter  tablespace DATA2 add datafile 'g:\oracle\hpcs\HPCS_DATA2_04.DBF' size 2048M autoextend off;
alter  tablespace DATA2 add datafile 'g:\oracle\hpcs\HPCS_DATA2_05.DBF' size 2048M autoextend off;
alter  tablespace DATA2 add datafile 'g:\oracle\hpcs\HPCS_DATA2_06.DBF' size 2048M autoextend off;

--alter  tablespace DATA2 add datafile 'g:\oracle\hpcs\HPCS_DATA2_07.DBF' size 2048M autoextend off;
--alter  tablespace DATA2 add datafile 'g:\oracle\hpcs\HPCS_DATA2_08.DBF' size 2048M autoextend off;
--alter  tablespace DATA2 add datafile 'g:\oracle\hpcs\HPCS_DATA2_09.DBF' size 2048M autoextend off;
--alter  tablespace DATA2 add datafile 'g:\oracle\hpcs\HPCS_DATA2_10.DBF' size 2048M autoextend off;

alter database tempfile 'G:\ORACLE\HPCS\HPCS_TEMP01.DBF' resize 1200M;

col name format a50
select name,bytes/1024/1024 MB from v$datafile order by 2 desc

select tablespace_name,sum(bytes)/1024/1024 GB from 
dba_data_files
group by tablespace_name;

select tablespace_name
, sum(bytes)/1024/1024 GB
from dba_temp_files
group by tablespace_name;
