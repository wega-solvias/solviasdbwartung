--------------------------------------------------  
-- Wega Informatik AG  
-- Purpose : HPCS enable Archiving
-- $Date$  
-- $Author$  
-- $HeadURL$  
-- $Revision$  
-------------------------------------------------- 
###############################################################################
# prepare Archiving
mkdir f:\archive\hpcs

###############################################################################
# enable Archiving
col host_name format a30
SELECT LOG_MODE FROM SYS.V$DATABASE;
select instance_name,host_name from v$instance;
v$archived_log

shutdown immediate;
startup mount;
alter database archivelog;
alter system set log_archive_start=TRUE                 scope=spfile;
alter system set log_archive_dest='f:\archive\hpcs'     scope=spfile;
alter system set log_archive_format='hpcs_%t-%S.arc'    scope=spfile;
shutdown immediate;
startup restrict;

alter system switch logfile;

###############################################################################
rman connect target /
CONFIGURE RETENTION POLICY TO RECOVERY WINDOW OF 7 DAYS;
CONFIGURE BACKUP OPTIMIZATION OFF; # default
CONFIGURE DEFAULT DEVICE TYPE TO DISK;
CONFIGURE CONTROLFILE AUTOBACKUP ON;
CONFIGURE CONTROLFILE AUTOBACKUP FORMAT FOR DEVICE TYPE DISK TO '\\so-f0002\DBBackup$\S2033\cs_backup\hpcs\cf%F_hpcs';
CONFIGURE DEVICE TYPE DISK PARALLELISM 1; # default
CONFIGURE DATAFILE BACKUP COPIES FOR DEVICE TYPE DISK TO 1; # default
CONFIGURE ARCHIVELOG BACKUP COPIES FOR DEVICE TYPE DISK TO 1; # default
CONFIGURE MAXSETSIZE TO 2147483648;
CONFIGURE SNAPSHOT CONTROLFILE NAME TO '\\SO-F0002\DBBACKUP$\S2033\CS_BACKUP\HPCS\SNAPCF_HPCS.F';
###############################################################################


archive log list
show parameter log_archive_dest
show parameter log_archive_format
show parameter log_archive_start

rman
connect target user/pass
show all



#################################################################################
# HPCS
Datenbank-Log-Modus              Archive-Modus
Automatische Archivierung             Aktiviert
Archivierungsziel            F:\archive\hpcs
-lteste Online-Log-Sequenz     107727
N�chste zu archivierende Log-Sequenz   107730
Aktuelle Log-Sequenz           107730

NAME                                 TYPE        VALUE
------------------------------------ ----------- ---------------------
log_archive_dest                     string      F:\archive\hpcs
log_archive_dest_state_1             string      enable
log_archive_dest_state_10            string      enable
log_archive_dest_state_2             string      enable
log_archive_dest_state_3             string      enable
log_archive_dest_state_4             string      enable
log_archive_dest_state_5             string      enable
log_archive_dest_state_6             string      enable
log_archive_dest_state_7             string      enable
log_archive_dest_state_8             string      enable
log_archive_dest_state_9             string      enable

NAME                                 TYPE        VALUE
------------------------------------ ----------- ---------------------
log_archive_dest_1                   string
log_archive_dest_10                  string
log_archive_dest_2                   string
log_archive_dest_3                   string
log_archive_dest_4                   string
log_archive_dest_5                   string
log_archive_dest_6                   string
log_archive_dest_7                   string
log_archive_dest_8                   string
log_archive_dest_9                   string

NAME                                 TYPE        VALUE
------------------------------------ ----------- ------------------------------
log_archive_format                   string      hpcs%T-%S.arc

NAME                                 TYPE        VALUE
------------------------------------ ----------- ------------------------------
log_archive_start                    boolean     TRUE

Mit Ziel-Datenbank verbunden: HPCS (DBID=2091384226)

RMAN> show all;

Kontrolldatei der Zieldatenbank wird anstelle des Recovery-Katalogs verwendet
RMAN-Konfigurationsparameter sind:
CONFIGURE RETENTION POLICY TO RECOVERY WINDOW OF 7 DAYS;
CONFIGURE BACKUP OPTIMIZATION OFF; # default
CONFIGURE DEFAULT DEVICE TYPE TO DISK;
CONFIGURE CONTROLFILE AUTOBACKUP ON;
CONFIGURE CONTROLFILE AUTOBACKUP FORMAT FOR DEVICE TYPE DISK TO '\\so-f0002\DBBackup$\S2033\cs_backup\hpcs\cf%F';
CONFIGURE DEVICE TYPE DISK PARALLELISM 1; # default
CONFIGURE DATAFILE BACKUP COPIES FOR DEVICE TYPE DISK TO 1; # default
CONFIGURE ARCHIVELOG BACKUP COPIES FOR DEVICE TYPE DISK TO 1; # default
CONFIGURE MAXSETSIZE TO 2147483648;
CONFIGURE SNAPSHOT CONTROLFILE NAME TO '\\SO-F0002\DBBACKUP$\S2033\CS_BACKUP\HPCS\SNAPCF_HPCS.F';